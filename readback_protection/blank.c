#include <stm8s.h>
#include <common.h>
#include <millis/tim4millis.h>
#include <eeprom.h>
#define SP_ON_RESET 0x3ff
#include <soft_reset.h>
#include <uart.h>

typedef struct pin{
	uint8_t number;
	uint16_t interval;
	uint16_t lastmillis;
	GPIO_TypeDef *gpio;
} pin;

pin pins[] = {
	{3, 100 +  20, 0, GPIOD},
	{1, 100 +  50, 0, GPIOD},
	{7, 100 +  70, 0, GPIOC},
	{6, 100 + 110, 0, GPIOC},
	{5, 100 + 130, 0, GPIOC},
	{4, 100 + 170, 0, GPIOC},
	{3, 100 + 190, 0, GPIOC},
	{4, 100 + 230, 0, GPIOB},
	{5, 100 + 290, 0, GPIOB},
	{3, 100 + 310, 0, GPIOA},
	{2, 100 + 370, 0, GPIOA},
	{1, 100 + 410, 0, GPIOA},
//	{6, 100 + 430, 0, GPIOD},
//	{5, 100 + 470, 0, GPIOD},
	{4, 100 + 530, 0, GPIOD},
};

DEFINE_TIMER_16(print_timer);

__at(0x4000) struct eepromdata_t {
	uint16_t test;
} eepromdata;


int wait(uint32_t i){
	volatile uint32_t m = i;
	while(m--){
		nop();
	}
	return i;
}

int main() {
	int i;
	CLK_INIT(CLOCK_SOURCE_INTERNAL, 0);
	TIM4_init();
	uart_init(921600);
	_putchar = uart_putchar;
	printf("%x\n", FLASH->IAPSR);

	for(i=0;i < sizeof(pins)/sizeof(pin);i++){
		pinmode(pins[i].number, pins[i].gpio, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);
	}

	__enable_interrupt();

	//LOCKDOWN :)!
	//if PUL flag is set it means the memory is preunlocked which happens during programming
	if(!(FLASH->IAPSR & FLASH_IAPSR_PUL) && !OPT->OPT0){
		FLASH->DUKR = 0xae;
		FLASH->DUKR = 0x56;
		FLASH->CR2 = FLASH_CR2_OPT; //allow write to option bytes
		FLASH->NCR2 = ~FLASH_NCR2_NOPT;
		OPT->OPT0 = 0xAA;
		//wait for programming to end (device to get locked)
		while(!(FLASH->IAPSR & FLASH_IAPSR_EOP)) { nop();};;
		printf("LOCKED!\n");
	}


	while(1){
		for(i=0;i < sizeof(pins)/sizeof(pin);i++)
			if(current_millis_uint16 - (uint32_t)pins[i].interval > pins[i].lastmillis){
				pins[i].gpio->ODR ^= (1 << pins[i].number);
				pins[i].lastmillis = current_millis_uint16;
			}
		if(TIMER_TICK_16(print_timer, 1000)){
			FLASH->DUKR = 0xae;
			FLASH->DUKR = 0x56;
//			while(!(FLASH->IAPSR & FLASH_IAPSR_PUL)) { nop();};
			eepromdata.test++;
			printf("%x\n", eepromdata.test);
			TIMER_REFRESH_16(print_timer);
		}

	}
}
