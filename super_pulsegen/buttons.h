#ifndef PCFBUTTONS_H
#define PCFBUTTONS_H

#include <stm8s.h>
#include <stdint.h>
#include <millis/tim4millis.h>
#include <pcf/pcf8574.h>
#include <common.h>

/**
 *  This provides button functionality with the pcf8574
 */

typedef uint8_t (*pcfbuttons_handler_t)(uint8_t state, uint8_t button);

typedef struct pcfbuttons_t {
	pcf8574_t *master;				/*!< The pcf8574_t where the buttons are */
	uint8_t interrupt_pin_mask;		/*!< 1 << interrupt_pin */
	GPIO_TypeDef *interrupt_port;	/*!< The port of the interrupt pin */
	uint16_t muxedbuttons;
	uint16_t prevmuxedbuttons;
	uint8_t buttons;				/*!< The buttons state */
	uint8_t prevbuttons;			/*!< The last buttons state */
	uint8_t pending;				/*!< Indicates if there is an event pending */
	uint8_t buttonmask;				/*!< The mask of pins where buttons are connected in the pcf8574 (1 is button, 0 is not) */
	uint8_t rows_mask;				/*!< The mask of the pins that are rows (1 is button, 0 is not)*/
	uint8_t cols_mask;				/*!< The mask of the pins that arse cols (1 is button, 0 is not) */
	uint8_t lastmillis;				/*!< The last 8bit millisecond counter value */
	pcfbuttons_handler_t handler;	/*!< The handler function */
} pcfbuttons_t;


int pcfbuttons_create(pcfbuttons_t *buttons, pcf8574_t *master, uint8_t buttonsmask, uint8_t rows_mask, uint8_t cols_mask, pcfbuttons_handler_t handler, uint8_t interrupt_pin, GPIO_TypeDef *interrupt_port);
void pcfbuttons_handle_interrupt(pcfbuttons_t *buttons);
int pcfbuttons_listen(pcfbuttons_t *buttons);
#endif
