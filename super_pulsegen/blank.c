#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stm8s.h>
#include <twi/twi.h>
#include <millis/tim4millis.h>
#include <common.h>
#include <uart.h>
#include <pcf/pcf8574.h>
#include "buttons.h"


uint32_t delay = 0;
uint8_t direction = 0;
uint16_t counter = 0;

pcf8574_t pcf;
pcfbuttons_t buttons;

void printbin(int in, int bits){
	char buff[33];
	uint16_t shiftmask;
	bits = bits*8;
	shiftmask = (1 << (bits-1));
	for(unsigned int i = 0; i < sizeof(buff); i++) buff[i] = 0;
	for(int i = 0; i < bits; i++){
		buff[i] = (in & (shiftmask >> i)) ? '1' : '0';
	}
	printf("\n%s\n", buff);
}

void interrupt_handler() __interrupt(IRQ_EXTIC)
{
	pcfbuttons_handle_interrupt(&buttons);
}

uint16_t magic = 0;

uint8_t button_handler(uint8_t state, uint8_t button){

	printf("button: %i, state: %i\n", button, state);
	magic ^= (1 << button);
	printbin(magic, 2);
	return 0;
}


int main() {
	CLK_INIT(CLK->ICKR, 0);
	TIM4_init();
	uart_init(115200);

	//enable peripherals
	pinmode(2, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);

	_putchar = uart_putchar;
	_getchar = uart_getchar;
	twi_init();
	printf("shittens\n");
	pcf8574_create(&pcf, 0);
	//EXTI->CR1 = EXTI_CR1_PCIS;
	pcfbuttons_create(&buttons, &pcf, 0xff, 0b00001111, 0b11110000, button_handler, 3, GPIOC);

//	pinmode(3, GPIOC, INPUT, INPUT_CR1_PULLUP, INPUT_CR2_INTERRUPT, 1);
	pinmode(4, GPIOB, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1);
	pinmode(5, GPIOB, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1);


	__enable_interrupt();


	//main loop and shit
	while(1){
		__wait_for_interrupt();
		pcfbuttons_listen(&buttons);
	}

}
