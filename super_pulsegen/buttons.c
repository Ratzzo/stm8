#include "buttons.h"
#include <stdio.h>

extern void printbin(int in, int bits);

int pcfbuttons_create(pcfbuttons_t *buttons, pcf8574_t *master, uint8_t buttonsmask, uint8_t rows_mask, uint8_t cols_mask, pcfbuttons_handler_t handler, uint8_t interrupt_pin, GPIO_TypeDef *interrupt_port){
	//set interrupt
	pinmode(interrupt_pin, interrupt_port, INPUT, INPUT_CR1_PULLUP, INPUT_CR2_INTERRUPT, HIGH);
	buttons->interrupt_pin_mask = 1 << interrupt_pin;
	buttons->interrupt_port = interrupt_port;
	buttons->prevbuttons = buttonsmask;
	buttons->buttonmask = buttonsmask;
	rows_mask &= buttonsmask;
	cols_mask &= buttonsmask;
	buttons->rows_mask = rows_mask;
	buttons->cols_mask = cols_mask;
	buttons->muxedbuttons = 0xffff;
	buttons->prevmuxedbuttons = 0xffff;
	buttons->buttons = cols_mask;
	buttons->pending = 0;
	buttons->master = master;
	buttons->handler = handler;
	pcf8574_setbyte(master, (pcf8574_getbyte(master) & ~rows_mask) | cols_mask);
	printbin(cols_mask , 1);
	return 0;
}


void pcfbuttons_handle_interrupt(pcfbuttons_t *buttons){
	__disable_interrupt();
	buttons->interrupt_port->CR2 &= ~buttons->interrupt_pin_mask;
	if(!buttons->pending){
		buttons->pending = 1;
		buttons->lastmillis = current_millis_uint8;
	}

	__enable_interrupt();
}

int pcfbuttons_listen(pcfbuttons_t *buttons){
	if(buttons->cols_mask && (~buttons->muxedbuttons || buttons->pending) && (uint8_t)(current_millis_uint8 - buttons->lastmillis) > 20){
		//poll because there is no way to detect individual buttons when multiplexed :(
		//perform an expensive row scan
		uint8_t rows = buttons->rows_mask;
		uint8_t cols = 0;
		uint8_t rowbit = 0;
		uint8_t colbit = 0;
		uint8_t byte = pcf8574_getbyte(buttons->master);
		uint16_t result = 0xffff;
		do {
			if(rows & 1){
				//At this point the buttons' state has to be stored in some way, as memory allocation is expensive in this context
				//given the maximum of 4 rows and 4 cols we can store all buttons in 16 bits (2 bytes)
				pcf8574_setbyte(buttons->master, (buttons->rows_mask & ~(1 << rowbit)) | buttons->cols_mask);
				colbit = 0;
				byte = pcf8574_getbyte(buttons->master);
				cols = buttons->cols_mask;
				do {
					if(cols & 1){
						result <<= 1;
						result |= (byte & (1 << colbit)) >> colbit;
					}
					colbit++;
				} while (cols >>= 1);
			}
			rowbit++;
		  } while (rows >>= 1);
		buttons->muxedbuttons = result;
		if(buttons->prevmuxedbuttons != buttons->muxedbuttons) {
			//dispatch
			if(buttons->handler){
				for(int i = 0; i < 16; i++){
					if(((~buttons->prevmuxedbuttons ^ ~buttons->muxedbuttons) >> i) & 1){
						buttons->handler(buttons->muxedbuttons >> i & 1, i);
					}
				}
			}

			buttons->prevmuxedbuttons = result;
		}
		pcf8574_setbyte(buttons->master, (pcf8574_getbyte(buttons->master) & ~buttons->rows_mask));
		buttons->buttons = pcf8574_getbyte(buttons->master) & buttons->buttonmask;
		buttons->interrupt_port->CR2 |= buttons->interrupt_pin_mask;
	}

	__disable_interrupt();
	if(buttons->pending && (uint8_t)(current_millis_uint8 - buttons->lastmillis) > 20){
		uint8_t i;
		buttons->buttons = pcf8574_getbyte(buttons->master) & buttons->buttonmask;
		if(buttons->handler)
			if(buttons->buttons != buttons->prevbuttons){
				for(i=0; i < 8; i++)
					if((((~buttons->prevbuttons ^ ~buttons->buttons) >> i) & 1) && !buttons->cols_mask){
							buttons->handler(buttons->buttons >> i & 1, i);
					}
				buttons->prevbuttons = buttons->buttons & buttons->buttonmask;
			}

		buttons->interrupt_port->CR2 |= buttons->interrupt_pin_mask;
		buttons->pending = 0;
	}
	__enable_interrupt();
	return 0;
}
