#include <common.h>
#include <stm8s.h>
#include <millis/tim4millis.h>
#include <math.h>


//
//  Timer 2 Overflow handler.
//
void TIM2_UPD_OVF_IRQHandler(void) __interrupt (13)
{
   //  Toggle Port D, pin 4.
    TIM2->SR1 |= TIM2_SR1_UIF;               //  Reset the interrupt otherwise it will fire again straight away.
}

//
//  Setup Timer 2 to generate a 20 Hz interrupt based upon a 16 MHz timer.

uint8_t r = 0;
uint8_t g = 0;
uint8_t b = 0;
//
void SetupTimer2()
{
	TIM2->PSCR = TIM2_PRESCALER_1;       //  Prescaler = 8.
	TIM2->ARRH = 0x00;       //  High byte of 50,000 . (freq)
	TIM2->ARRL = 0xFF;       //  Low byte of 50,000.
	//TIM3
    TIM2->CCER1 |= TIM2_CCER1_CC1P;    //  Active high.
    TIM2->CCER1 |= TIM2_CCER1_CC2P;    //  Active high.
    TIM2->CCER2 |= TIM2_CCER2_CC3P;    //  Active high.
    TIM2->CCER1 |= TIM2_CCER1_CC1E;    //  Enable compare mode for channel 1
    TIM2->CCER1 |= TIM2_CCER1_CC2E;    //  Enable compare mode for channel 1
    TIM2->CCER2 |= TIM2_CCER2_CC3E;    //  Enable compare mode for channel 1
    TIM2->CCMR1 |= TIM2_CCMR_OCM;    //  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
    TIM2->CCMR2 |= TIM2_CCMR_OCM;    //  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
    TIM2->CCMR3 |= TIM2_CCMR_OCM;    //  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
  //  TIM2->IER |= TIM2_IER_UIE;       //  Enable the update interrupts.
    TIM2->CR1 |= TIM2_CR1_CEN;       //  Finally enable the timer.
}

#define AMPL 255

int16_t sine(int32_t x){
//this returns -10000 to 10000 on x = 0 to 36000
	int16_t result = 0;
	x %= 35999;
	if(x <= 18000)
	result = x*(18000-x)/8100;
	if(x > 18000)
	result = (x - 36000)*(x - 18000)/8100;

	return result;
}

DEFINE_TIMER_8(active);

int32_t accum = 0;
uint32_t incmillis;
int main()
{
	__disable_interrupt();
	CLK_INIT(CLOCK_SOURCE_INTERNAL, 0)
	pinmode(1, GPIOA, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);

    //GPIOD->ODR ^= (1 << 3);
//	SetupTimer2();
	TIM4_init();
    incmillis = millis();

	__enable_interrupt();
    while (1)
    {
/*		if(millis() - incmillis > 1){
			int32_t m;
            TIM2->CCR1H = 0x00;      //  High byte of 12,500
            TIM2->CCR1L = r;      //  Low byte of 12,500
            TIM2->CCR2H = 0x00;      //  High byte of 12,500
            TIM2->CCR2L = g;      //  Low byte of 12,500
            TIM2->CCR3H = 0x00;      //  High byte of 12,500
            TIM2->CCR3L = b;      //  Low byte of 12,500
			accum += 30;
			m = 127+sine(accum)/79;
//			r = m;
			g = 127+sine(accum)/79;
			r = 127+sine(accum+18000)/79;;
			b = 127+sine(accum+27000)/79;;
            incmillis = millis();
		} */
		if(TIMER_TICK_8(active, 100)){
			GPIOA->ODR ^= (1 << 1);
			TIMER_REFRESH_8(active);
		}
    }
}
