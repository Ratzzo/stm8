#include <stm8s.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "serial.h"
#include "proto.h"

//#define _tty "\\\\.\\COM21"
#define _tty "/dev/ttyUSB0"

int ret;
serial_t ser;
proto_t proto;
uint8_t data[300] = {0x10, 0x22, 0x40};

uint8_t printdata(char *data, uint32_t size){
	while(size--){
	printf("%.2X ", (uint8_t)*data++);
	}
	printf("\n");
	return 1;
}

uint8_t buffer1[0x1000];
uint8_t buffer2[0x1000];
uint8_t buffer3[256];
//unit tests

int get_shared_address(proto_t *proto){
	int result = 0;
	uint16_t addr;
	printf("\t%x\n", result = proto_getsharedaddr(proto, &addr));
	printf("\tshared address is 0x%x\n", addr);
	return result;
}

int writeread_bytes(proto_t *proto, uint16_t addr, uint16_t size){
	int ret;
	for(int i = 0; i < 256; i++){
		buffer1[i] = (uint8_t)i;
	}
	printf("\tproto_write: %i\n", ret = proto_write(proto, addr, buffer1, size));
	if(ret) return ret;
	printf("\tproto_read: %i\n", ret = proto_read(proto, addr, buffer2, size));
	if(ret) return ret;

	//compare written and read back
	if(!memcmp(buffer1, buffer2, size))
		printf("\tread and written memory are the same :)\n");
	else
	{
		printf("\tread and written memory differ :(\n");
		return 1;
	}

	return 0;
}


int main(int argc, char *argv[]){
	printf("creating proto... %s\n", (ret = proto_create(&proto, _tty)) ? "FAIL" : "PASS" );
	if(ret) printf("error code: %i");

	printf("writeread 255 bytes...\n");
	printf("%s\n", (ret = writeread_bytes(&proto, 0x200, 0x100)) ? "FAIL" : "PASS" );
	printf("get_shared_address...\n");
	printf("%s\n", (ret = get_shared_address(&proto)) ? "FAIL" : "PASS" );



	proto_destroy(&proto);


	return 1;
}
