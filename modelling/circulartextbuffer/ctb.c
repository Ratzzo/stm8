#include "ctb.h"

//some helpers that may not be available
int _strnlen(const char *s, int maxlen){
	int len = 0;
	while(*s++ != '\0' && ++len != maxlen){};
	return len;
}

/**
 * @brief ctb_create initializes a ctb structure
 * @param ct the ctb structure
 * @param size the size of the structure
 * @param truncateat at what index to truncate strings put
 * @return 0 on success
 */
int ctb_create(ctb_t *ct, int size, int truncateat){
	ct->mem = (char*)malloc(size);
	ct->size = size;
	if(!ct->mem) return 1;
	memset(ct->mem, 0, size);
	ct->truncateat = truncateat;
	ct->n_entries = 0;
	ct->index = 0;

	return 0;
}

int _ctb_recount(ctb_t *ct){
	int len = 0;
	int i = 0;
	while(i < ct->size){
	if(ct->mem[i] == '\0' && ct->mem[i+1] != '\0') len++;
	i++;
	}
	return len+1;
}

int ctb_put(ctb_t *ct, char *str){
	int stlen;
	int probe = 0;
	//check for overflow
	if((stlen = _strnlen(str, ct->truncateat)+1) + ct->index > ct->size){
		memset(&ct->mem[ct->size - ct->index], 1, ct->size-ct->index);
		ct->index = 0;
		while(probe < ct->size && ct->mem[probe] != '\0'){
			ct->mem[probe] = 0x00; //fill memory until the next 0
			probe++;
		}
	}
	strncpy(&ct->mem[ct->index], str, stlen);
	ct->index += stlen;
	probe = ct->index;
	while(probe < ct->size && ct->mem[probe] != '\0'){
		ct->mem[probe] = 0x00; //fill memory until the next 0
		probe++;
	}
	ct->n_entries = _ctb_recount(ct);
	return 0;
}
char *ctb_get(ctb_t *ct, int index){
	int probe = ct->index;
	//probe back until 0 is found
	if(index < ct->n_entries){
		do {
			probe -= 2;
			while(probe >= 0 && ct->mem[probe]){
				probe--;
			}
			if(probe < -1){
				probe = ct->size-1;
				index++;
				while(probe > 0 && !ct->mem[probe]){
					probe--;
				}
			}
		} while (index--);

		return &ct->mem[probe+1];
	}

	return "null";
}

/**
 * @brief ctb_destroy destroys a ctb structure
 * @param ct the structure to destroy
 * @return 0 on success
 */
int ctb_destroy(ctb_t *ct){
	return 0;
}
