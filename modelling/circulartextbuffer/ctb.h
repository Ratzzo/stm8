//Implementation of a circular n sized text buffer for embedded devices
#ifndef CTB_H
#define CTB_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct ctb_t {
	char *mem;
	int n_entries;
	int truncateat;
	int index;
	int size;
} ctb_t;

int ctb_create(ctb_t *ct, int size, int truncateat);
int ctb_put(ctb_t *ct, char *str);
char *ctb_get(ctb_t *ct, int index);
int ctb_destroy(ctb_t *ct);

#endif
