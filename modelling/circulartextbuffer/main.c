#include <stdio.h>
#include <stdlib.h>
#include "ctb.h"

int dumpbuff(char *buff, int size){
for(int i = 0; i < size; i++){
	printf("%.2X ", buff[i]);
	if(!((i+1)%8)) printf("\n");
}
printf("\n");
return 0;
}



ctb_t ctb;

int main(int argc, char **argv){
	ctb_create(&ctb, 50, 30);
	ctb_put(&ctb, "baloney");
	ctb_put(&ctb, "magical");
	ctb_put(&ctb, "mittens");
	ctb_put(&ctb, "kickbox");
	ctb_put(&ctb, "kickoff");
	ctb_put(&ctb, "te");
	ctb_put(&ctb, "tse");
	ctb_put(&ctb, "s2s");
	ctb_put(&ctb, "te");
	ctb_put(&ctb, "t-");
	ctb_put(&ctb, "t0");
	ctb_put(&ctb, "t1");
	ctb_put(&ctb, "t2sasdas");
	printf("%i\n", ctb.n_entries);
	dumpbuff(ctb.mem, ctb.size);
	for(int i = 0; i < ctb.n_entries; i++){
		printf("%s\n", ctb_get(&ctb, i));
	}
	ctb_destroy(&ctb);
	return 0;
}
