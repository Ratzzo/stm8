#include "ccontrols.h"

cbutton::cbutton(int X, int Y, int W, int H, const char *L): Fl_Button(X, Y, W, H, L) {

}

void cbutton::draw() {
  if (type() == FL_HIDDEN_BUTTON) return;
  Fl_Color col = value() ? selection_color() : color();
  if(Fl::focus() == this)
	draw_box(value() ? (down_box()?down_box():fl_down(box())) : box(), col-1);
  else
	draw_box(value() ? (down_box()?down_box():fl_down(box())) : box(), col);

  draw_backdrop();
  if (labeltype() == FL_NORMAL_LABEL && value()) {
    Fl_Color c = labelcolor();
    labelcolor(fl_contrast(c, col));
    draw_label();
    labelcolor(c);
  } else draw_label();
//  if (Fl::focus() == this) draw_focus();
}

cbutton::~cbutton(){
}

cchoice::cchoice(int X, int Y, int W, int H, const char *L): Fl_Choice(X, Y, W, H, L) {

}

void cchoice::draw() {
  Fl_Boxtype btype = Fl::scheme() ? FL_UP_BOX		// non-default uses up box
                                  : FL_DOWN_BOX;	// default scheme uses down box
  int dx = Fl::box_dx(btype);
  int dy = Fl::box_dy(btype);

  // Arrow area
  int H = h() - 2 * dy;
  int W = Fl::is_scheme("gtk+")    ? 20 :			// gtk+  -- fixed size
          Fl::is_scheme("gleam")   ? 20 :			// gleam -- fixed size
          Fl::is_scheme("plastic") ? ((H > 20) ? 20 : H)	// plastic: shrink if H<20
                                   : ((H > 20) ? 20 : H);	// default: shrink if H<20
  int X = x() + w() - W - dx;
  int Y = y() + dy;

  // Arrow object
  int w1 = (W - 4) / 3; if (w1 < 1) w1 = 1;
  int x1 = X + (W - 2 * w1 - 1) / 2;
  int y1 = Y + (H - w1 - 1) / 2;

  if (Fl::scheme()) {
    // NON-DEFAULT SCHEME

    // Draw widget box
    draw_box(FL_BORDER_BOX, color());

    // Draw arrow area
    fl_color(active_r() ? labelcolor() : fl_inactive(labelcolor()));
    if (Fl::is_scheme("plastic")) {
      // Show larger up/down arrows...
      fl_polygon(x1, y1 + 3, x1 + w1, y1 + w1 + 3, x1 + 2 * w1, y1 + 3);
      fl_polygon(x1, y1 + 1, x1 + w1, y1 - w1 + 1, x1 + 2 * w1, y1 + 1);
    } else {
      // Show smaller up/down arrows with a divider...
      x1 = x() + w() - 13 - dx;
      y1 = y() + h() / 2;
      fl_polygon(x1, y1 - 2, x1 + 3, y1 - 5, x1 + 6, y1 - 2);
      fl_polygon(x1, y1 + 2, x1 + 3, y1 + 5, x1 + 6, y1 + 2);

      fl_color(fl_darker(color()));
      fl_yxline(x1 - 7, y1 - 8, y1 + 8);

      fl_color(fl_lighter(color()));
      fl_yxline(x1 - 6, y1 - 8, y1 + 8);
    }
  } else {
    // DEFAULT SCHEME

    // Draw widget box
    if (fl_contrast(textcolor(), FL_BACKGROUND2_COLOR) == textcolor()) {
      draw_box(btype, FL_BACKGROUND2_COLOR);
    } else {
      draw_box(btype, fl_lighter(color()));
    }

    // Draw arrow area
    draw_box(FL_UP_BOX,X,Y,W,H,color());
    fl_color(active_r() ? labelcolor() : fl_inactive(labelcolor()));
    fl_polygon(x1, y1, x1 + w1, y1 + w1, x1 + 2 * w1, y1);
  }

  W += 2 * dx;

  // Draw menu item's label
  if (mvalue()) {
    Fl_Menu_Item m = *mvalue();
    if (active_r()) m.activate(); else m.deactivate();

    // Clip
    int xx = x() + dx, yy = y() + dy + 1, ww = w() - W, hh = H - 2;
    fl_push_clip(xx, yy, ww, hh);

    if ( Fl::scheme()) {
      Fl_Label l;
      l.value = m.text;
      l.image = 0;
      l.deimage = 0;
      l.type = m.labeltype_;
      l.font = m.labelsize_ || m.labelfont_ ? m.labelfont_ : textfont();
      l.size = m.labelsize_ ? m.labelsize_ : textsize();
      l.color= m.labelcolor_ ? m.labelcolor_ : textcolor();
      if (!m.active()) l.color = fl_inactive((Fl_Color)l.color);
      fl_draw_shortcut = 2; // hack value to make '&' disappear
      l.draw(xx+3, yy, ww>6 ? ww-6 : 0, hh, FL_ALIGN_LEFT);
      fl_draw_shortcut = 0;
//      if ( Fl::focus() == this ) draw_focus(box(), xx, yy, ww, hh);
    }
    else {
      fl_draw_shortcut = 2; // hack value to make '&' disappear
      m.draw(xx, yy, ww, hh, this, Fl::focus() == this);
      fl_draw_shortcut = 0;
    }

    fl_pop_clip();
  }

  // Widget's label
  draw_label();
}

cchoice::~cchoice(){
}



