#ifndef CBUTTON_H
#define CBUTTON_H
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <FL/Enumerations.H>
#include <FL/Fl_Button.H>
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Window.H>     // for fl_set_spot()
#include <FL/Fl_Device.H>     // for fl_graphics_driver
#include <FL/Fl_Choice.H>     // for fl_graphics_driver

class cbutton : public Fl_Button {
	public:
		cbutton(int X, int Y, int W, int H, const char *L);
		~cbutton();
		void draw();
};

class cchoice : public Fl_Choice {
	public:
		cchoice(int X, int Y, int W, int H, const char *L);
		~cchoice();
		void draw();
};

#endif
