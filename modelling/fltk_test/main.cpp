#include <stdio.h>
#include <stdlib.h>
#include "form.h"
#include <cbutton.h>
#include <transitions.h>
#include <FL/fl_draw.H>

Fl_Double_Window *window;

void button_cb(Fl_Widget *widget, void *data){
	printf("%x, %f\n", widget, button->value());
}

int main(int argc, char **argv){
	window = make_window();
	Fl::scheme("gtk+");
	Fl::visual(FL_RGB);
	window->damage();
	window->show();
	Fl::run();
	return 0;
}
