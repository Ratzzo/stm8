#include "cbutton.h"
#include <transitions.h>


rgba_color_t rgba_color(uint8_t r, uint8_t g, uint8_t b, uint8_t a){
	rgba_color_t rgb;
	rgb.r = r;
	rgb.g = g;
	rgb.b = b;
	rgb.a = a;
	return rgb;
}

void bin_image_fillrect(uint8_t *imagedata, int x, int y, int w, int h, rgba_color_t color, int linew){
	rgba_color_t *pixels = (rgba_color_t*)imagedata;
	int line = 0;
	for(int _y = y; _y < x+h; _y++){
		line += linew;
		for(int _x = x; _x < x+w; _x++){
			pixels[line + _x] = color;
		}
	}

}


rgb_color_t rgb_color(uint8_t r, uint8_t g, uint8_t b){
	rgb_color_t rgb;
	rgb.r = r;
	rgb.g = g;
	rgb.b = b;
	return rgb;
}

void bin_image_fillrect(uint8_t *imagedata, int x, int y, int w, int h, rgb_color_t color, int linew){
	rgb_color_t *pixels = (rgb_color_t*)imagedata;
	int line = 0;
	for(int _y = y; _y < x+h; _y++){
		line += linew;
		for(int _x = x; _x < x+w; _x++){
			pixels[line + _x] = color;
		}
	}
}

void cbutton::button_up_frame(int x, int y, int w, int h, rgb_color_t color) {
//  gtk_color(fl_color_average(FL_BLACK, c, 0.5));
//  fl_xyline(x + 2, y + 1, x + w - 3);
//  fl_yxline(x + 1, y + 2, y + h - 3);
	bin_image_fillrect(imagedata, 1, 1, w-2, h-2, color, this->w());
//	bin_image_fillrect(imagedata, 0, 0, w/2, h/2, color, this->w());

	fl_draw_image(imagedata, x, y, w, h, 3, 0);
}

/*
float timeval = 0;
void handler_1(void *t){
	cbutton *but = (cbutton *)t;
	but->normalcolor.r = (0.1*255*sin(timeval)			+ 255*0.8);
	but->normalcolor.g = (0.1*255*sin(timeval+M_PI/2)	+ 255*0.8);
	but->normalcolor.b = (0.1*255*sin(timeval+M_PI)		+ 255*0.8);

//	but->damage();
	but->redraw();
	timeval += 0.01;
	Fl::add_timeout(0.01, handler_1, t);
} */

cbutton::cbutton(int X, int Y, int W, int H, const char *L): Fl_Button(X, Y, W, H, L) {
	printf("shit!\n");
	imagesize = W*H*sizeof(rgb_color_t);
	imagedata = (uint8_t*)malloc(imagesize);
	bg = (uint8_t*)malloc(imagesize);
	firstdraw = 0;
//	Fl::add_timeout(0.01, handler_1,(void*)this);
//	printf("fuck\n");
}

int cbutton::handle(int event){
	switch (event)
	{
	case FL_ENTER:
	normalcolor = rgb_color(0xff, 0xff, 0xff);
	redraw();
	break;
	case FL_LEAVE:
	normalcolor = rgb_color(0xf0, 0xf0, 0xf0);
	redraw();
	break;
	}
	return Fl_Button::handle(event);
}

void cbutton::draw(){
//	if(!firstdraw && ++firstdraw) fl_read_image(bg, x(), y(), w(), h(), 0);
	memcpy(imagedata, bg, imagesize);
//if(value())
//	button_up_frame(x(), y(), w(), h(), ;
//else
	button_up_frame(x(), y(), w(), h(), normalcolor);


  if (type() == FL_HIDDEN_BUTTON) return;
  Fl_Color col = value() ? selection_color() : color();
  //draw_box(value() ? (down_box()?down_box():fl_down(box())) : box(), col);
  draw_backdrop();
  if (labeltype() == FL_NORMAL_LABEL && value()) {
    Fl_Color c = labelcolor();
    labelcolor(fl_contrast(c, col));
    draw_label();
    labelcolor(c);
  } else draw_label();
//  if (Fl::focus() == this) draw_focus();
}

cbutton::~cbutton(){
	free(imagedata);
	free(bg);
}
