#ifndef CBUTTON_H
#define CBUTTON_H
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <FL/Enumerations.H>
#include <FL/Fl_Button.H>
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Window.H>     // for fl_set_spot()
#include <FL/Fl_Device.H>     // for fl_graphics_driver

#pragma pack(push, 1)
typedef struct rgb_color_t {
	uint8_t r, g, b;
}	rgb_color_t;

typedef struct rgba_color_t {
	uint8_t r, g, b, a;
}	rgba_color_t;
#pragma pack(pop)

class cbutton : public Fl_Button {
	private:
		int imagesize;
		uint8_t *imagedata;
		uint8_t  *bg; //background
		uint8_t firstdraw;
	public:
		rgb_color_t normalcolor;
		cbutton(int X, int Y, int W, int H, const char *L);
		~cbutton();
		int handle(int event);
		void button_up_frame(int x, int y, int w, int h, rgb_color_t color);
		void draw();
};

#endif
