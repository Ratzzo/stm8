#include <stdio.h>
#include <stdlib.h>
#include <packedintarray.h>

packedintarray_t p;

int printbits(uint32_t x){
	for(int i = 24; i >= 0; i--){
		printf("%i",!!(x & (1 << i)));
	}
	printf("\n");
}

uint8_t consistent;



int main(int argc, char **argv){
	uint32_t get;
	for(uint32_t m = 0; m < 100; m++){
		packedintarray_create(&p, 21,  10);
		for(uint32_t i = 0; i < p.length; i++){
			packedintarray_set(&p, i, i);
			printf("set ");
			printbits(i);
			if(!((get = packedintarray_get(&p, i)) == i%(1 << p.intbits)))
				consistent = 0;
			printf("get ");
			printbits(get);
		}
		packedintarray_destroy(&p);
	}

	return 0;
}
