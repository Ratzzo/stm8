#ifndef DARKPNGH
#define DARKPNGH

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char uchar;

#pragma pack(push, 1)

typedef struct datasize32_t{
	uint32_t size;
	uint8_t *data;
} datasize32_t;

typedef struct{
	uint32_t width, height; //width and height of the image in pixels
	uint32_t bitdepth; //bitdepth... 8 assuming r8 g8 b8 a8
	char *data; //raw data of the image, total size = width*height*bitdepth/8 and... remember to free this after use
	int color_type; //24 or 32 bits
} png_t;

typedef struct{
	uint8_t r,g,b;
} png_rgb_t; //24

typedef struct{
	uint8_t r,g,b,a;
} png_rgba_t; //32

#pragma pack(pop)

#ifdef __cplusplus
extern "C"{
#endif

png_rgb_t *png_rgb_tp(uchar r, uchar g, uchar b); //generate a color pointer from rgb value, it should be freed after use

unsigned int dark_readpngfromfile(png_t *out, char *fname);
unsigned int dark_readpngfrommemory(png_t *out, char *in);
    /*
    out = pointer to already created png_t object;
    in = pointer to the in png file in memory
    this function returns 0 on no error
    */
unsigned int dark_writepngtofile(png_t *in, char *fname);
unsigned int dark_writepngtomemory(png_t *in, datasize32_t *out); //encodes only 8 bit rgba
    /*
    out = pointer to png_tOut object;
    in = pointer to an already created png_t_memorydata object
    this function returns 0 on no error
    */
unsigned int dark_reallocatepng24to32(png_t *inout);
unsigned int dark_clearpng(png_t *in);
#ifdef __cplusplus
};
#endif
#endif // DARKPNGH