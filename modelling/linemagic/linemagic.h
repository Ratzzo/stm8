#ifndef LINEMAGIC_H
#define LINEMAGIC_H

#include <stdint.h>

#ifdef __cplusplus
extern "C"{
#endif


int linemagic_pack(uint8_t *in, uint8_t *out, int insize);
int linemagic_unpack(uint8_t *in, uint8_t *out, int insize);

#ifdef __cplusplus
}
#endif

#endif // LINEMAGIC_H
