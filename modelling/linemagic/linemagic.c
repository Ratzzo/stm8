#include "linemagic.h"


int linemagic_pack(uint8_t *in, uint8_t *out, int insize){
    int i;
    int o;
    for(i = 0; i < insize; i++){
        *out = 0;
        for(o = 0; o < 8; o++){
            *out |= ((!!(*in)) << o);
            in++;
        }
        out++;
    }

return -1;
}

int linemagic_unpack(uint8_t *in, uint8_t *out, int insize){
    int i;
    int o;
    for(i = 0; i < insize; i++){
        for(o = 0; o < 8; o++){
            *out = (*in & (1 << o)) ? 0xff : 0x00 ;
            out++;
        }
        in++;
    }
return -1;
}
