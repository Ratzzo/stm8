#include <stdio.h>
#include <stdlib.h>
#include "darkpng.h"
#include "linemagic.h"

int main(int argc, char **argv){
	png_t p;
	if(argc < 4){
		printf("Chupilsen PNG to LL (Laser Line)\n", argc);
		printf("Usage: %s <infile> <outfile> <scanlinenumber>\n", argv[0]);
		printf("\n");
		return 1;
	}

	//check file existence.
	uint8_t fin;
	fin = dark_readpngfromfile(&p, argv[1]);
	if(fin){
		printf("failed to open file %s in read mode\n", argv[1]);
		return 2;
	}

	FILE *fout;
	fout = fopen(argv[2], "wb");
	if(!fout){
		printf("failed to open file %s in write mode\n", argv[2]);
		return 3;
	}

	int l;
	if(!sscanf(argv[3], "%i", &l)){
		printf("invalid scanline number\n");
		return 4;
	}

    if(l > p.height-1){
        printf("line exceeds file height.\n");
        return 5;
    }

//    FILE *fdeb1;
//	fdeb1 = fopen("a", "wb");

//    FILE *fdeb2;
//	fdeb2 = fopen("b", "wb");

    printf("bitdepth: %u\n",p.bitdepth);
    printf("color_type: %u\n",p.color_type);
    printf("width: %u\n",p.width);
    printf("height: %u\n",p.height);
    printf("\n");

    uint32_t linesize = ((p.bitdepth/8)*p.width);
    uint8_t *line = (uint8_t*)(p.data + linesize*l);
    uint8_t *outbuff;

//    fwrite(line, sizeof(uint8_t), linesize, fdeb1);

    printf("converting...");
    //pack the line in a format understandable by the stm8
    outbuff = (uint8_t*)malloc(linesize/8);
    linemagic_pack(line, outbuff, linesize);
    fwrite(outbuff, sizeof(uint8_t), linesize/8, fout);
 //   linemagic_unpack(outbuff, line, linesize/8);
  //  fwrite(line, sizeof(uint8_t), linesize, fdeb2);
    printf("DONE.\n\n");

    printf("output size: %u\n", linesize/8);

	return 0;
}
