#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


int main(int argc, char **argv){
	uint8_t b = 0b11000110;
	for(int i = 0; i < 8; i++){
		printf("%i\n", (b >>= 1) & 1);
	}
	
	return 0;
}
