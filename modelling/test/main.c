#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>

int16_t sine(int32_t x){
//this returns -10000 to 10000 on x = 0 to 36000
	int16_t result = 0;
	x %= 35999;
	if(x <= 18000)
	result = x*(18000-x)/8100;
	if(x > 18000)
	result = (x - 36000)*(x - 18000)/8100;

	return result;
}

uint32_t x = 0;

int main(int argc, char **argv){
	while(1){
	printf("%x\n", 0x40 + (sine(x) * 0x40)/10000);
	x += 30;
	usleep(10000);
	}
	return 0;
}
