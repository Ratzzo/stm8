#include <stdlib.h>
#include "darkpng.h"
#include <png.h>


png_rgb_t *png_rgb_tp(uchar r, uchar g, uchar b)
{
    png_rgb_t *temp = (png_rgb_t*)malloc(sizeof(png_rgb_t));
    if(!temp) return 0;
    temp->r = r;
    temp->g = g;
    temp->b = b;
    return temp;
}

void customdataread(png_structp pngptr, png_bytep data, png_size_t length)
{
    char **readpos = (char**)png_get_io_ptr(pngptr); //restore readpos

    memmove((void*)data, (void*)*readpos, length); //movemem
    //memmove()
    *readpos += length; //increment readpos
}

unsigned int dark_writepngtofile(png_t *in, char *fname)
{
    FILE *fp = fopen(fname, "wb");
    if(!fp) return 1;
    datasize32_t out;
    unsigned int rv;

    rv = dark_writepngtomemory(in, &out);
    if(rv) return rv;
    fwrite(out.data, 1, out.size, fp);
    free(out.data);
    fclose(fp);
    return 0;
}

unsigned int dark_readpngfromfile(png_t *out, char *fname)
{
    FILE *fp = fopen(fname, "rb");
    if(!fp) return 1;
    fseek(fp, 0, SEEK_END);
    unsigned int fsize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    if(!fsize)
    {
        fclose(fp);
        return 2;
    }
    char *buff = (char*)malloc(fsize);
    if(!buff)
    {
        fclose(fp);
        return 3;
    }
    fread(buff, 1, fsize, fp);
    fclose(fp);
    unsigned int rv;
    rv = dark_readpngfrommemory(out, buff);
    free(buff);
    if(rv) return rv;
    return 0;
}

unsigned int dark_reallocatepng24to32(png_t *inout) //this reallocate the raw output from 24 to 32 bits, this returns != 0 on error
{
    if(inout->bitdepth != 24) return 0; //not a real error
    png_rgba_t *newblock = (png_rgba_t*)malloc(inout->width * inout->height * 32 / 8);
    if(newblock == 0) return 2;
    png_rgb_t *oldblock = (png_rgb_t*)(inout->data);

    unsigned int x, y, width = inout->width, height = inout->height;

    for(y = 0; y < height; y++)
        for(x = 0; x < width; x++)
        {

            newblock[y*width+x].r = oldblock[y*width+x].r;
            newblock[y*width+x].g = oldblock[y*width+x].g;
            newblock[y*width+x].b = oldblock[y*width+x].b;
            newblock[y*width+x].a = 255;
        }
    free(inout->data);
    inout->bitdepth = 32;
    inout->data = (char*)newblock;
    return 0;
};

unsigned int dark_readpngfrommemory(png_t *out, char *in)
{
    out->data = 0;
    char **readpos = (char**)malloc(sizeof(char*)); //set new pointer for readbuffer, this new pointer should be incremented
    if(readpos == 0)
    {
        return 4;
    }
    *readpos = in;
    if(png_sig_cmp((png_byte*)*readpos, 0, 8)) //compare signature from the one in memory, if they're not equal the file isnt png and return 4
    {
        free(readpos);
        return 1; //not png
    }
    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL); //create read struct
    if(!png_ptr)
    {
        free(readpos);
        return 2; //if struct not created return
    }

    png_infop info_ptr = png_create_info_struct(png_ptr); //create info struct, if this is not created..
    if(!info_ptr)
    {
        free(readpos);
        png_destroy_read_struct(&png_ptr, 0, 0); //we destroy it and then
        return 3; //return
    }

    if(setjmp(png_jmpbuf(png_ptr)))  //setjmp for jumping here for error handling
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, 0); //destroy the two structs allocated and
        free(readpos);
        return 5; //return
    }

    png_set_read_fn(png_ptr, readpos, customdataread); //set read function

    png_set_sig_bytes(png_ptr, 0); //set signature bytes to 0 since we haven't incremented readpos when we readed the signature

    png_read_info(png_ptr, info_ptr); //read the info structure

    unsigned int width = png_get_image_width(png_ptr, info_ptr);
    unsigned int height = png_get_image_height(png_ptr, info_ptr);

    unsigned int bitdepth = png_get_bit_depth(png_ptr, info_ptr);
    unsigned int channels = png_get_channels(png_ptr, info_ptr);

    unsigned int colortype = png_get_color_type(png_ptr, info_ptr);


    switch(colortype)
    {
    case PNG_COLOR_TYPE_PALETTE:
    {
        png_set_palette_to_rgb(png_ptr);
        break;
    }
    case PNG_COLOR_TYPE_GRAY:
    {
        if(bitdepth < 8)
        {
            png_set_expand_gray_1_2_4_to_8(png_ptr);
            bitdepth = 8;
        }
        break;
    }


    }

    if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
    {
        png_set_tRNS_to_alpha(png_ptr);
        channels++;
    }

    if(bitdepth == 16)
    {
        png_set_strip_16(png_ptr);
    }

    png_bytep *rowptrs = (png_bytep*)malloc(sizeof(png_bytep)*height);
    if(rowptrs == 0)
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, 0); //destroy the two structs allocated and
        free(readpos);
        return 6;
    }

    unsigned int imagerawsize = width*height*channels*bitdepth/8;
    png_byte *imagedata = (png_byte*)malloc(imagerawsize);//allocate image data
    if(imagedata == 0)
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, 0); //destroy the two structs allocated and
        free(readpos);
        free(rowptrs);
        return 7;
    }

    unsigned int currentrow = 0;

    for(currentrow=0; currentrow < height; currentrow++)
    {
        rowptrs[currentrow] = imagedata+(currentrow*width*channels*bitdepth/8); //fillrows
//printf("%x\n", rowptrs[currentrow]);
    }

    png_read_image(png_ptr, rowptrs);
//        printf("%u %u\n", *(char**)png_get_io_ptr(png_ptr) - in, width*height*4);


    out->data = (char*)imagedata;
    out->width = width;
    out->height = height;
    out->bitdepth = bitdepth*channels;

    png_destroy_read_struct(&png_ptr, &info_ptr, 0);
    free(rowptrs);
    free(readpos);

    return 0;
}


#define PNG_BLOCKSIZE = 1024

typedef struct
png_t_writedesc{
    char *data;
    unsigned int size, cbs; //cbs = current block size
} png_t_writedesc;

#define darkpngblocksize 4096

void customdatawrite(png_structp pngptr, png_bytep data, png_size_t length)
{
//printf("caca");
    png_t_writedesc *writedesc = (png_t_writedesc*)png_get_io_ptr(pngptr); //restore writedesc
    unsigned int oldcbs = writedesc->cbs;

    while(writedesc->size+length > writedesc->cbs)
        writedesc->cbs += darkpngblocksize;

    if(writedesc->cbs != oldcbs)
        writedesc->data = realloc(writedesc->data, writedesc->cbs);

    if(!writedesc->data) goto nope; //to avoid corruption

    memmove(writedesc->data + writedesc->size, data, length); //movemem

nope:

    writedesc->size += length; //increment size
    //writedesc->data += length;

}

unsigned int dark_writepngtomemory(png_t *in, datasize32_t *out){
    png_t_writedesc *writedesc = (png_t_writedesc*)malloc(sizeof(png_t_writedesc)); //set new pointer for readbuffer, this new pointer should be incremented
    writedesc->data = 0;
    writedesc->size = 0;
    writedesc->cbs = 0;
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL); //create read struct
    if(!png_ptr)
    {
        free(writedesc);
        return 2; //if struct not created return
    }

    png_infop info_ptr = png_create_info_struct(png_ptr); //create info struct, if this is not created..
    if(!info_ptr)
    {
        free(writedesc);
        png_destroy_read_struct(&png_ptr, 0, 0); //we destroy it and then
        return 3; //return
    }

    if(setjmp(png_jmpbuf(png_ptr)))  //setjmp for jumping here for error handling
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, 0); //destroy the two structs allocated and
        free(writedesc);
        return 5; //return
    }


    png_set_write_fn(png_ptr, writedesc, customdatawrite, 0);



    png_set_IHDR(png_ptr, info_ptr, in->width, in->height, in->bitdepth, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

    png_write_info(png_ptr, info_ptr);

    png_bytep *rowptrs = (png_bytep*)malloc(sizeof(png_bytep)*in->height);
    if(rowptrs == 0)
    {
        png_destroy_write_struct(&png_ptr, &info_ptr); //destroy the two structs allocated and
        free(writedesc);
        return 6;
    }



    unsigned int currentrow;

    for(currentrow=0; currentrow < in->height; currentrow++)
    {
        rowptrs[currentrow] = (png_bytep)in->data+(currentrow*in->width*4*in->bitdepth/8); //fillrows
//printf("%x\n", rowptrs[currentrow]);
    }




    png_write_image(png_ptr, rowptrs);



    writedesc->data = realloc(writedesc->data, writedesc->size);
    if(writedesc->data == 0)
    {
        png_destroy_write_struct(&png_ptr, &info_ptr);
        free(rowptrs);
        free(writedesc);
        return 7;
    }



    out->data = writedesc->data;
    out->size = writedesc->size;

    png_destroy_write_struct(&png_ptr, &info_ptr);

    free(rowptrs);
    free(writedesc);

    return 0;
}

unsigned int dark_clearpng(png_t *in){
free(in->data);
return 0;
}
