#ifndef	PNG_UTIL_H
#define	PNG_UTIL_H

#include <stdint.h>
#include <png.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#pragma pack(push, 1)

typedef struct png_datasize32_t{
	uint32_t size;
	uint8_t *data;
} png_datasize32_t;

typedef struct png_t{
	uint32_t width, height; //width and height of the image in pixels
	uint32_t bitdepth; //bitdepth... 8 assuming r8 g8 b8 a8
	uint8_t channels; //number of channels
	char *data; //raw data of the image, total size = width*height*bitdepth/8 and... remember to free this after use
	int color_type; //24 or 32 bits
} png_t;

typedef struct png_rgb_t {
	uint8_t r,g,b;
} png_rgb_t; //24

typedef struct png_rgba_t{
	uint8_t r,g,b,a;
} png_rgba_t; //32


#pragma pack(pop)

unsigned int pngutil_readpngfrommemory(png_t *out, char *in);
unsigned int pngutil_writepngtomemory(png_t *in, png_datasize32_t *out);

unsigned int pngutil_writepngtofile(png_t *in, char *fname);
unsigned int pngutil_readpngfromfile(png_t *out, char *fname);


#endif
