#include <stdio.h>
#include <stdlib.h>
#include "pngutil.h"

png_t png = {0};

int dumpmem(char *mem, int size){
for(int i = 0; i < size;i++){
	if(i%8 == 0){
	printf("\n");
	}
	printf("%.2X ", (unsigned char)mem[i]);
}

}

int main(int argc, char **argv){
	int ret = pngutil_readpngfromfile(&png, "dice.png");
	printf("width: %i\n", png.width);
	printf("height: %i\n", png.height);
	printf("color_type: %i\n", png.color_type);
	printf("bitdepth: %i\n", png.bitdepth);
	printf("data: %x\n", png.data);
	printf("ret: %i\n", ret);
//	dumpmem(png.data + 0x20000, 100);
	pngutil_writepngtofile(&png, "asd22.png");
	return 0;
}
