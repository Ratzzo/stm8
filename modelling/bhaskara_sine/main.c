#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// gcc main.c -o main && ./main && gnuplot -p -e "plot 'output' using 1:2 with lines"

int16_t sine(int32_t x){
//this returns -10000 to 10000 on x = 0 to 36000
	int16_t result = 0;
	x %= 35999;
	if(x <= 18000)
	result = x*(18000-x)/8100;
	if(x > 18000)
	result = (x - 36000)*(x - 18000)/8100;
	
	return result;
}

int main(){
	FILE *fp = fopen("output", "wb");
	fprintf(fp, "#X Y\n");
	for(int i = 0; i <= 40000; i++){
		int16_t data = sine(i*10);
		fprintf(fp, "%i %i\n", i, data); 
	}
	fclose(fp);
	return 0;
}
