#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define size 4

uint8_t ring[size];
uint8_t start = 0;
uint8_t end = 0;

uint8_t ring_put(uint8_t data){
	printf("put %u %u\n", (end+1)%size, data);
	end++;
	end %= size;
	ring[end] = data;
}

uint8_t ring_get(){
 if(start == end) return 0;
 uint8_t data;
 start++;
 start %= size;

 data = ring[start];
 printf("get %u %u\n", start, data);
}


int main(int argc, char **argv){

	ring_put(10);
	ring_put(20);
	ring_put(30);
	ring_get();
	ring_get();
	ring_put(10);
	ring_put(20);
	ring_get();
	ring_get();
	ring_put(10);
	ring_put(20);
	ring_get();
	ring_get();
	return 0;
}
