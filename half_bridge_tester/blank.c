#include <stm8s.h>
#include <millis/tim4millis.h>
#include <delay.h>
#include <common.h>
#include <spi595/spi_595.h>

uint32_t running_millis;

typedef struct pin_state_t {
	GPIO_TypeDef *GPIO;
	uint8_t number;
	uint8_t state;
} pin_state_t;

uint8_t stepcounter = 0;


pin_state_t steps[] = {
	{GPIOC, 7, 1}, //led
	{GPIOD, 3, 1},
	{GPIOD, 3, 0}, //bottom fet
	{GPIOC, 7, 0}, //led
	{GPIOD, 2, 1},
	{GPIOD, 2, 0}, //top fet
};

int main()
{
//	uint8_t counter = 0;
	uint32_t step_millis;
	CLK_INIT(CLK->ICKR, 0);
	TIM4_init();

//	EXTI->CR1 = 0;// |= EXTI_CR1_PDIS & 0b11000000; //full mask, interrupt on rising and falling edges

	pinmode(4, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, HIGH);
	pinmode(2, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, LOW);
	pinmode(3, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, LOW);
	pinmode(7, GPIOC, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, LOW);


	running_millis = 0;
	step_millis = 0;

	__enable_interrupt();
	// Loop
	while(1){
		if(millis() - running_millis > 100){
			GPIOD->ODR ^= (1 << 4);
			running_millis = millis();
		}

		if(millis() - step_millis > 1){
			pin_state_t *step;
			step = &steps[stepcounter];

			if(step->state)
				step->GPIO->ODR |= (1 << step->number);
			else
				step->GPIO->ODR &= ~(1 << step->number);

			stepcounter++;
			stepcounter %= sizeof(steps)/sizeof(pin_state_t);
			step_millis = millis();
		}

	}
}
