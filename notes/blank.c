#include <stdint.h>
#include <stm8s.h>
#include "tim4millis.h"
#include <math.h>
#include <common.h>
#include <eeprom.h>

#define	 C 65406
#define CS 69296
#define	 D 73416
#define DS 77782
#define	 E 82407
#define	 F 87307
#define FS 92499
#define	 G 97999
#define GS 103830
#define	 A 110000
#define AS 116540
#define	 B 123470

uint32_t frequencies[] = {
	C /2,	//0
	CS/2,	//1
	D /2,	//2
	DS/2,	//3
	E /2,	//4
	F /2,	//5
	FS/2,	//6
	G /2,	//7
	GS/2,	//8
	A /2,	//9
	AS/2,	//10
	B /2,	//11
	C ,		//12
	CS,		//13
	D ,		//14
	DS,		//15
	E ,		//16
	F ,		//17
	FS,		//18
	G ,		//19
	GS,		//20
	A ,		//21
	AS,		//22
	B ,		//23
	C *2,	//24
	CS*2,	//25
	D *2,	//26
	DS*2,	//27
	E *2,	//28
	F *2,	//29
	FS*2,	//30
	G *2,	//31
	GS*2,	//32
	A *2,	//33
	AS*2,	//34
	B *2,	//35
	C *4,	//36
	CS*4,	//37
	D *4,	//38
	DS*4,	//39
	E *4,	//40
	F *4,	//41
	FS*4,	//42
	G *4,	//43
	GS*4,	//44
	A *4,	//45
	AS*4,	//46
	B *4,	//47
	C *8,	//48
	CS*8,	//49
	D *8,	//50
	DS*8,	//51
	E *8,	//52
	F *8,	//53
	FS*8,	//54
	G *8,	//55
	GS*8,	//56
	A *8,	//57
	AS*8,	//58
	B *8,	//59
	0		//60

};



#define	 C_4(x) x+0
#define CS_4(x) x+1
#define	 D_4(x) x+2
#define DS_4(x) x+3
#define	 E_4(x) x+4
#define	 F_4(x) x+5
#define FS_4(x) x+6
#define	 G_4(x) x+7
#define GS_4(x) x+8
#define	 A_4(x) x+9
#define AS_4(x) x+10
#define	 B_4(x) x+11
#define	 C_5(x) x+12
#define CS_5(x) x+13
#define	 D_5(x) x+14
#define DS_5(x) x+15
#define	 E_5(x) x+16
#define	 F_5(x) x+17
#define FS_5(x) x+18
#define	 G_5(x) x+19
#define GS_5(x) x+20
#define	 A_5(x) x+21
#define AS_5(x) x+22
#define	 B_5(x) x+23
#define	 C_6(x) x+24
#define CS_6(x) x+25
#define	 D_6(x) x+26
#define DS_6(x) x+27
#define	 E_6(x) x+28
#define	 F_6(x) x+29
#define FS_6(x) x+30
#define	 G_6(x) x+31
#define GS_6(x) x+32
#define	 A_6(x) x+33
#define AS_6(x) x+34
#define	 B_6(x) x+35
#define	 C_7(x) x+36
#define CS_7(x) x+37
#define	 D_7(x) x+38
#define DS_7(x) x+39
#define	 E_7(x) x+40
#define	 F_7(x) x+41
#define FS_7(x) x+42
#define	 G_7(x) x+43
#define GS_7(x) x+44
#define	 A_7(x) x+45
#define AS_7(x) x+46
#define	 B_7(x) x+47
#define	 C_8(x) x+48
#define CS_8(x) x+49
#define	 D_8(x) x+50
#define DS_8(x) x+51
#define	 E_8(x) x+52
#define	 F_8(x) x+53
#define FS_8(x) x+54
#define	 G_8(x) x+55
#define GS_8(x) x+56
#define	 A_8(x) x+57
#define AS_8(x) x+58
#define	 B_8(x) x+59
#define	 PAU(x) 60




uint8_t pattern[] = {
	G_5(1),
	PAU(1),
	F_5(1),
	PAU(1),
	E_5(1),
	PAU(1),
	G_5(1),
	PAU(1),
	PAU(1),
	PAU(1),
	E_5(1),
	PAU(1),
	PAU(1),
	PAU(1),
	PAU(1),
	PAU(1),
	E_5(1),
	PAU(1),
	E_5(1),
	PAU(1),
	E_5(1),
	PAU(1),
	C_5(1),
	PAU(1),
	A_4(1),
	PAU(1),
	PAU(1),
	PAU(1),
	PAU(1),
	PAU(1),
	PAU(1),
	PAU(1),
	G_5(1),
	PAU(1),
	F_5(1),
	PAU(1),
	E_5(1),
	PAU(1),
	G_5(1),
	PAU(1),
	PAU(1),
	PAU(1),
	E_5(1),
	PAU(1),
	PAU(1),
	PAU(1),
	PAU(1),
	PAU(1),
	D_5(1),
	PAU(1),
	E_5(1),
	PAU(1),
	E_5(1),
	PAU(1),
	E_5(1),
	PAU(1),
	PAU(1),
	PAU(1),
	PAU(1),
	PAU(1),
	PAU(1),
	PAU(1),
	PAU(1),
	PAU(1),
};






void set_freq(uint32_t freq){
	uint16_t f_half;
	uint16_t f;
	uint8_t h;
	uint8_t l;
	uint8_t hh;
	uint8_t hl;
	uint32_t constant = 0xef9020*100; //product of frequency and
	if(freq == 0){
		TIM2->CR1 = 0;
		return;
	}
	else{

		TIM2->CR1 |= TIM2_CR1_CEN;
	}

	f = constant/freq;
	f_half = f/2;
	h  = ((uint8_t*)(&f))[0];
	l  = ((uint8_t*)(&f))[1];
	hh = ((uint8_t*)(&f_half))[0];
	hl = ((uint8_t*)(&f_half))[1];
	TIM2->ARRH = h;       //  High byte of 50,000 . (freq)
	TIM2->ARRL = l;       //  Low byte of 50,000.
	TIM2->CCR1H = hh;
	TIM2->CCR1L = hl;
	TIM2->CCR2H = hh;
	TIM2->CCR2L = hl;
	TIM2->CCR3H = hh;
	TIM2->CCR3L = hl;
}

//

//
void SetupTimer2()
{
	TIM2->PSCR = TIM2_PRESCALER_1;       //  Prescaler = 8.
	TIM2->ARRH = 0x10;       //  High byte of 50,000 . (freq)
	TIM2->ARRL = 0x00;       //  Low byte of 50,000.
	TIM2->CCER2 |= TIM2_CCER2_CC3P;    //  Active high.
	TIM2->CCER2 |= TIM2_CCER2_CC3E;    //  Enable compare mode for channel 1.
//	TIM2->CCER1 |= TIM2_CCER1_CC2P;    //  Active high.
	TIM2->CCER1 |= TIM2_CCER1_CC2E;    //  Enable compare mode for channel 1.
//	TIM2->CCER1 |= TIM2_CCER1_CC1P;    //  Active high.
	TIM2->CCER1 |= TIM2_CCER1_CC1E;    //  Enable compare mode for channel 1.
    TIM2->CCMR3 |= TIM2_CCMR_OCM;    //  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
	TIM2->CCMR1 |= TIM2_CCMR_OCM;    //  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
	TIM2->CCMR2 |= TIM2_CCMR_OCM;    //  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
  //  TIM2->IER |= TIM2_IER_UIE;       //  Enable the update interrupts.
	TIM2->CR1 |= TIM2_CR1_CEN;       //  Finally enable the timer.
}

//
//  Main program loop.
//

#define AMPL 100
uint8_t play = 0;

void button() __interrupt(IRQ_EXTI2)
{
	__disable_interrupt();
	play = 1;
	__enable_interrupt();
}

uint32_t ind = 0;

uint32_t active_millis = 0;
uint32_t dance_millis = 0;

int main()
{


    __disable_interrupt();
	CLK_INIT(CLOCK_SOURCE_INTERNAL, 0);
	SetupTimer2();
	set_freq(0);
	TIM4_init();
	active_millis = millis();

	pinmode(3, GPIOC, INPUT, INPUT_CR1_PULLUP, INPUT_CR2_INTERRUPT, 0);
	pinmode(3, GPIOA, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);
	pinmode(3, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1);
	pinmode(4, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1);

//	EXTI->CR1 &= ~EXTI_CR1_PDIS;
//	EXTI->CR1 &= ~EXTI_CR1_PCIS;
	EXTI->CR1 |= 0b10010000;

	__enable_interrupt();
	__halt();

    while (1)
    {
		if(play && current_millis - active_millis >	100){
			set_freq(frequencies[pattern[ind++]]);
			if(ind >= sizeof(pattern)/sizeof(uint8_t)){
				ind = 0;
				play = 0;
				set_freq(0);
			}
			active_millis = current_millis;
		}
		if(!play)
			__halt();
	}

}
