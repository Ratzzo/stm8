OUTDIR=out/
SHELL=bash #ugh arduino
OUTPUT=$(OUTDIR)$(OBJECT)
BASEDIR?=../

.PHONY: all clean flash dumpram viewram editram unlock

DEVICE?=STM8S103

Build: all
LIBDIR=$(BASEDIR)lib/

SYSDEPS+=$(shell $(BASEDIR)tools/infer_dependencies $(LIBDIR) sys)
LOCDEPS+=$(shell $(BASEDIR)tools/infer_dependencies . local)

all: printsize

cleanBuild: clean


clean:
	rm -f *.o
	rm -rf $(OUTDIR)
	rm -f *.bin
	rm -f unlock
	@bash -c "$(foreach source, $(LIBS), rm -f $(basename $(source)).o;)"

Flash: flash

ifdef LOCK
DOUNLOCK=unlock
endif

flash: $(OUTPUT) $(DOUNLOCK)
	@stm8flash -c$(PROG) -p$(PART) -w $(OUTPUT); if [ $$? == 0 ]; then echo Device flashed successfully.; else echo -e \\nError flashing device.; exit 1; fi;
ifeq ($(LOCK), 1)
	@echo -e "Locking back..."
	@stm8flash -cstlinkv2 -sopt -p$(PART) -w $(LIBDIR)$(PART)_opt_lock
	@rm unlock
endif

LOCDEPS+=$(addprefix ./, $(CSOURCES))
SYSRELS=$(sort $(SYSDEPS:$(LIBDIR)%.c=$(OUTDIR)%.rel))
LOCRELS=$(sort $(LOCDEPS:./%.c=$(OUTDIR)%.rel))

REFS+=$(LOCRELS)
REFS+=$(SYSRELS)

#$(error $(LOCDEPS))
#compile local dependencies
$(LOCRELS): $(OUTDIR)%.rel : %.c
	@mkdir -p $(dir $@)
	$(SDCC) -D $(DEVICE)=1 -DLOCK=$(LOCK) -c $(INCLUDEDIRS) $(CFLAGS) -lstm8 -mstm8 $< -o $@

#compile system dependencies (ie, stuff shared among all projects)
$(SYSRELS): $(OUTDIR)%.rel : $(LIBDIR)%.c
	@mkdir -p $(dir $@)
	$(SDCC) -D $(DEVICE)=1 -DLOCK=$(LOCK) -c $(INCLUDEDIRS) $(CFLAGS) -lstm8 -mstm8 $< -o $@

#link
$(OUTPUT): $(LOCRELS) $(SYSRELS)
	$(SDCC) -D $(DEVICE)=1 -o $(OUTPUT) $(INCLUDEDIRS)  -lstm8 -mstm8 --out-fmt-ihx $(CFLAGS) $(LDFLAGS) $(REFS)
	@sdobjcopy -I ihex $(OUTPUT) -O binary $(OUTDIR)out.bin

printsize: $(OUTPUT)
	@echo -e "\n\n\n\n\n"
	@ls -l $(OUTDIR)out.bin
	@echo -e "\n\n\n\n\n"

unbrick:
	@stm8flash -c$(PROG) -sopt  -p$(PART) -u
	@stm8flash -c$(PROG) -sopt -p$(PART) -w $(LIBDIR)$(PART)_opt_lock
	@stm8flash -c$(PROG) -sopt  -p$(PART) -u

unlock:
	@echo -e "Unlocking..."
	@stm8flash -c$(PROG) -sopt  -p$(PART) -u
	@touch unlock

dumpflash:
	stm8flash -c$(PROG) -p$(PART) -sflash -r flash.bin
	cp flash.bin flash-$(shell date +%s).bin

#ram stuff
dumpram:
	stm8flash -c$(PROG) -p$(PART) -n -sram -r ram.bin
	cp ram.bin ram-$(shell date +%s).bin

viewram: dumpram
	@okteta ram.bin 2> /dev/null 1> /dev/null &
editram: dumpram
	@okteta ram.bin 2> /dev/null 1> /dev/null
	stm8flash -c$(PROG) -p$(PART) -n -sram -w ram.bin

#eeprom stuff
dumpeeprom:
	stm8flash -c$(PROG) -p$(PART) -seeprom -r eeprom.bin
	cp eeprom.bin eeprom-$(shell date +%s).bin
vieweeprom: dumpeeprom
	@okteta eeprom.bin 2> /dev/null 1> /dev/null &

editeeprom: dumpeeprom
	@okteta eeprom.bin 2> /dev/null 1> /dev/null
	stm8flash -c$(PROG) -p$(PART) -seeprom -w eeprom.bin
