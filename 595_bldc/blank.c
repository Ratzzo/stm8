#include <stm8s.h>
//#include <millis/tim4millis.h>
#include <delay.h>
#include <common.h>
#include <spi595/spi_595.h>

uint32_t counter_millis = 0;
uint32_t commutation_millis = 0;
spi595_t s595;

uint32_t c_millis = 0;

void tim4_irq() __interrupt (23)
{
    //increase 1, for millis() function
    c_millis ++;

//	TIM4_ClearITPendingBit(TIM4_IT_UPDATE);
    TIM4->SR1 = (uint8_t)(~TIM4_IT_UPDATE);
    __no_operation();
}


uint32_t millis(void)
{
    return c_millis;
}


void TIM4_init(void)
{

        /* TIM4 configuration:
    - TIM4CLK is set to 16 MHz, the TIM4 Prescaler is equal to 128 so the TIM1 counter
    clock used is 16 MHz / 128 = 125 000 Hz
    - With 125 000 Hz we can generate time base:
      max time base is 2.048 ms if TIM4_PERIOD = 255 --> (255 + 1) / 125000 = 2.048 ms
      min time base is 0.016 ms if TIM4_PERIOD = 1   --> (  1 + 1) / 125000 = 0.016 ms
    - In this example we need to generate a time base equal to 1 ms
    so TIM4_PERIOD = (0.001 * 125000 - 1) = 124 */

    /* Time base configuration */
    //TIM4_TimeBaseInit(TIM4_PRESCALER_128, 124);
        TIM4->PSCR = (uint8_t)(TIM4_PRESCALER_128);
        TIM4->ARR = (uint8_t)(10); //
        TIM4->SR1 = (uint8_t)(~TIM4_FLAG_UPDATE); //clear Update flag
        TIM4->IER |= TIM4_IER_UIE; //Update Interrupt Enable
        TIM4->CR1 |= TIM4_CR1_CEN; //enable TIM4

}



#define ENABLE_OUTPUT() GPIOC->ODR &= ~(1 << 3); //this is OE of the 595 just as a security measure
#define DISABLE_OUTPUT() GPIOC->ODR |= (1 << 3);

uint8_t invert_mask = 0b01010100; //all off
uint8_t cindex = 0;

uint8_t commutation_table[] = {
    //UH UL VH VL WH WL XX XX
    0b00011000,
    0b10010000,
    0b10000100,
    0b00100100,
    0b01100000,
    0b01001000,
};


uint32_t accelsteps[] = {
    1,
    100,
    400,
    600,
    7000,

};


uint32_t adelay = 500;
uint32_t protodelay = 30;
uint32_t delay;
uint32_t msdelay = 1000;
uint8_t accelstep = 0;
uint8_t closed = 0;

uint8_t startup = 1; //motor is starting up
int main()
{
    uint8_t byte = 1;
    GPIOC->DDR &= ~(1 << 3); //set !OE as input to avoid shoot through
    GPIOC->CR1 |= (1 << 3); //pushpull
    GPIOC->CR2 |= (1 << 3); //10mhz
    GPIOC->ODR |= (1 << 3); //defaults high
    GPIOC->DDR |= (1 << 3); //set as output

    CLK_INIT(CLK->ICKR, 0);
    SPI_INIT_MASTER(SPI_BAUDRATEPRESCALER_2, SPI_CLOCKPOLARITY_HIGH, SPI_CLOCKPHASE_1EDGE);
    TIM4_init();

    __enable_interrupt();
    spi595_create(&s595, GPIOA, 3);
    spi595_setbyte(&s595, 0x00 ^ invert_mask);
    ENABLE_OUTPUT()

    spi595_setbyte(&s595, (s595.status & 0b11) | (commutation_table[0] ^ invert_mask)); //start with first step
    //loop
    while(1){
        int i = 0;
        if(startup){
            if(millis() > 5000){ //wait for positioning
                startup = 0;
            }

        }
        else while(1)
            if(!closed){
                if(millis() - counter_millis >  accelsteps[accelstep]){ //decrement step delay
                    spi595_setbyte(&s595, s595.status ^ 0b10);

                     counter_millis = millis();
					 if(msdelay > 22){
                        if(msdelay == 300 || msdelay == 150 || msdelay == 80)
                            accelstep++;
                        msdelay--;
                     }
                     else
                    {
//						closed = 1;
//						spi595_setbyte(&s595, 0x00 ^ invert_mask);
                     }

                }

                if(millis() - commutation_millis >=  msdelay){
                   //spi595_setbyte(&s595, (s595.status & 0b11) | (0x00 ^ invert_mask));
                   //_delay_ms(100);
                   spi595_setbyte(&s595, (s595.status & 0b11) | (commutation_table[cindex++] ^ invert_mask));
                    if(cindex >= sizeof(commutation_table)){
                        cindex = 0;
                    }
                    commutation_millis = millis();
                }
            }
        else while(1){

        }

    }
}
