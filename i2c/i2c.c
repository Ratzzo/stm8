#include <twi/twi.h>
#include <millis/tim4millis.h>
#include <pcf/pcf8574.h>
#include <pcf/7seg.h>
#include <common.h>

seg7_t segments;

int16_t biet = 0;

int main()
{
    uint32_t blinkmillis;
    uint32_t countermillis;
    uint32_t dotmillis;
    uint8_t buffer[10];

    __disable_interrupt();

    IWDG_INIT(IWDG_Prescaler_32, 0xff);

    blinkmillis = millis();
    countermillis = blinkmillis;
    dotmillis = blinkmillis;

    CLK_INIT(CLK->ICKR, 0)


    buffer[0] = 0x00;

    twi_init();


    seg7_create(&segments, 0, 1);

    TIM4_init();


    __enable_interrupt();

    while (1)
    {

//        __wait_for_interrupt();
        if(millis() >= countermillis + 100){
            seg7_setnumber(&segments, biet);
            biet++;
            if(biet == -1)
                biet = 9;
            countermillis = millis();
        }
        if(millis() >= dotmillis + 250){
            pcf8574_setbyte(&segments.digit0, segments.digit0.state ^ 1);
        //    pcf8574_setbyte(&segments.digit1, segments.digit1.state ^ 1);
            dotmillis = millis();
        }
        IWDG_FEED();
    }
}
