#include <stm8s.h>
#include <common.h>
#include <millis/tim4millis.h>

typedef struct pin{
	uint8_t number;
	uint16_t ton;
	uint16_t toff;
	uint16_t offset;
	uint16_t interval;
	uint16_t last_millis;
	GPIO_TypeDef *gpio;
} pin;

pin pins[] = {
	{2, 10, 2000, 0, 500, 0, GPIOD},
	{3, 10, 2000, 0, 1000, 0, GPIOA},
	{4, 60, 2000, 0, 1500, 0, GPIOB},
};




int main() {
	int i;
	CLK_INIT(CLOCK_SOURCE_INTERNAL, 0);
	TIM4_init();

	for(i=0;i < sizeof(pins)/sizeof(pin);i++){
		pinmode(pins[i].number, pins[i].gpio, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1);
	}

	__enable_interrupt();
	while(1){
		for(i=0;i < sizeof(pins)/sizeof(pin);i++)
			if(TIMER_TICK_16(pins[i].last, pins[i].interval)){
				if(pins[i].gpio->ODR & (1 << pins[i].number)){
					pins[i].interval = pins[i].ton;
				}
				else
				{
					pins[i].interval = pins[i].toff;
				}
				pins[i].gpio->ODR ^= (1 << pins[i].number);
				//TIMER_REFRESH_16(pins[i].last);
				pins[i].last_millis = current_millis_uint16 + pins[i].offset;
			}
	}
}
