#include <stm8s.h>
#include <common.h>
#include <millis/tim4millis.h>

typedef struct step_t{
	GPIO_TypeDef *gpio;
	uint8_t pin;
} step_t;

typedef struct commutation_t {
	uint8_t s1, s2, s3, s4;
} commutation_t;

step_t steps[] = {
	{GPIOD, 4},
	{GPIOA, 3},
	{GPIOD, 5},
	{GPIOD, 6},
};

commutation_t commutation_table[] = {
	{1, 0, 0, 0},
	{0, 1, 0, 0},
	{0, 0, 1, 0},
	{0, 0, 0, 1},
};



step_t *previous_step = 0;

uint8_t step_counter = 0;


DEFINE_TIMER_8(active);
DEFINE_TIMER_16(tester);

int main() {
	CLK_INIT(CLOCK_SOURCE_INTERNAL, 0);
	TIM4_init();
	pinmode(5, GPIOB, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);

	//C3 (UP ->), C4 (DOWN <-) -> buttons
	pinmode(3, GPIOC, INPUT, INPUT_CR1_PULLUP, INPUT_CR2_NOINTERRUPT, 0);
	pinmode(4, GPIOC, INPUT, INPUT_CR1_PULLUP, INPUT_CR2_NOINTERRUPT, 0);

	for(int i = 0; i < sizeof(steps)/sizeof(step_t); i++){
		pinmode(steps[i].pin, steps[i].gpio, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);
	}
	__enable_interrupt();

	//D4, D5, D6, A3 -> steps

	while(1){
		if(TIMER_TICK_8(active, 254)){
			GPIOB->ODR ^= (1 << 5);
			TIMER_REFRESH_8(active);
		}
		if(!(GPIOC->IDR & (1 << 4)) && TIMER_TICK_16(tester, 3)){
			step_t *step = &steps[step_counter];
			if(previous_step)
				previous_step->gpio->ODR &= ~(1 << previous_step->pin);
			step->gpio->ODR |= (1 << step->pin);


			previous_step = step;
			step_counter++;
			step_counter %= sizeof(steps)/sizeof(step_t);


			TIMER_REFRESH_16(tester);
		}

		if(!(GPIOC->IDR & (1 << 3)) && TIMER_TICK_16(tester, 3)){
			step_t *step = &steps[step_counter];
			if(previous_step)
				previous_step->gpio->ODR &= ~(1 << previous_step->pin);
			step->gpio->ODR |= (1 << step->pin);

			previous_step = step;
			step_counter--;
			if(step_counter > 4){
				step_counter = 3;
			}
			step_counter %= sizeof(steps)/sizeof(step_t) + 1;


			TIMER_REFRESH_16(tester);
		}

		if((GPIOC->IDR & (1 << 3)) && (GPIOC->IDR & (1 << 4))){
		for(int i = 0; i < sizeof(steps)/sizeof(step_t); i++)
			steps[i].gpio->ODR &= (1 << steps[i].pin);
		}

	}

}
