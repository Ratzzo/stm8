#include <stm8s.h>
#include <millis/tim4millis.h>
#include <delay.h>
#include <common.h>
#include <ws2801/ws2801.h>
#include <math.h>

uint8_t lastcounter;
uint8_t /*__at (0x0000)*/ counter; //this preserves variables trought resets.

uint8_t ilock;
uint32_t counter_millis;
uint32_t welp_millis;


int main()
{
//	uint8_t counter = 0;
	float accum = 0;
	uint8_t r = 0, g = 0, b = 0;
	ws2801_t ws;
	CLK_INIT(CLOCK_SOURCE_INTERNAL, 0);
	SPI_INIT_MASTER(SPI_BAUDRATEPRESCALER_2, SPI_CLOCKPOLARITY_LOW, SPI_CLOCKPHASE_1EDGE);
	TIM4_init();

//	EXTI->CR1 = 0;// |= EXTI_CR1_PDIS & 0b11000000; //full mask, interrupt on rising and falling edges

	pinmode(3, GPIOA, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, HIGH);
	counter = 0;
	ws2801_create(&ws);

	counter_millis = 0;
	__enable_interrupt();
	// Loop
	while(1){
		__enable_interrupt();
		if(millis() - counter_millis > 10){
			counter++;
			if((GPIOA->ODR & (1 << 3))){
				accum += 0.030f;
				r = (127 + 127*sinf(accum + PI));
				g = (127 + 127*sinf((accum + PI)*2));
				b = (127 + 127*sinf((accum + PI)*4));
				ws2801_setdata(&ws, r, g, b);
			}
			else
			{
				ws2801_setdata(&ws, 0, 0, 0);
			}
			//GPIOA->ODR |= (1 << 3);

			//GPIOA->ODR &= ~(1 << 3);
			counter_millis = millis();
		}
		if(millis() - welp_millis > 1000){
			GPIOA->ODR ^= (1 << 3);
			welp_millis = millis();
		}
	}
}
