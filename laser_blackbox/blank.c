#include <stdint.h>
#include <stm8s.h>
#include <millis/tim4millis.h>
#include <math.h>
#include <common.h>
#include <eeprom.h>
#include <uart.h>
#include <ring_buffer.h>
#include <uart_proto/proto.h>
#include <string.h>

#include "serializer.h"
#define SP_ON_RESET 0x3ff
#include <soft_reset.h>

#define PIN_LASER 2			//laser
#define GPIO_LASER GPIOD	//laser

#define PIN_POLY 5			//polygon mirror S/S
#define GPIO_POLY GPIOB		//polygon mirror S/S

#define PIN_CLK 4			//CLK initialized
#define GPIO_CLK GPIOB		//CLK initialized

#define PIN_EOLD 4			//end of line detector (photodiode)
#define GPIO_EOLD GPIOC		//end of line detector (photodiode)

#define PIN_PLOCK 4			//polygon mirror locked
#define GPIO_PLOCK GPIOD	//polygon mirror


void (*_serializedata)(uint8_t *data, uint8_t size) = SERIALIZER_ADDRESS;

struct {
	uint8_t reset_pending;
	uint8_t set_mirror_freq_pending;
	uint16_t mirror_frequency;
	uint8_t line[200];
	uint8_t clk_delay;
} shared_data;


	DEFINE_TIMER_8(active);
	DEFINE_RING(rx, 16);

void set_freq(uint16_t freq){
	uint16_t f_half;
	uint16_t f;
	uint8_t h;
	uint8_t l;
	uint8_t hh;
	uint8_t hl;
	uint32_t constant = 0xef9020; //product of frequency and
	f = constant/freq;
	f_half = f/2;
	h  = ((uint8_t*)(&f))[0];
	l  = ((uint8_t*)(&f))[1];
	hh = ((uint8_t*)(&f_half))[0];
	hl = ((uint8_t*)(&f_half))[1];
	TIM2->ARRH = h;       //  High byte of 50,000 . (freq)
	TIM2->ARRL = l;       //  Low byte of 50,000.
	TIM2->CCR3H = hh;
	TIM2->CCR3L = hl;
}

void SetupTimer2()
{
	TIM2->PSCR = TIM2_PRESCALER_1;       //  Prescaler = 8.
	TIM2->ARRH = 0x10;       //  High byte of 50,000 . (freq)
	TIM2->ARRL = 0x00;       //  Low byte of 50,000.
	TIM2->CCER2 |= TIM2_CCER2_CC3P;    //  Active high.
	TIM2->CCER2 |= TIM2_CCER2_CC3E;    //  Enable compare mode for channel 1.
	TIM2->CCMR3 |= TIM2_CCMR_OCM;    //  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
  //  TIM2->IER |= TIM2_IER_UIE;       //  Enable the update interrupts.
	TIM2->CR1 |= TIM2_CR1_CEN;       //  Finally enable the timer.
}


void mirror_locked() __interrupt(IRQ_EXTI3)
{
	__disable_interrupt();
	shared_data.clk_delay = 50;
	__enable_interrupt();
}

uint16_t addr = 0;
uint8_t doswap = 0;
DEFINE_TIMER_8(swap);

void end_of_line() __interrupt(IRQ_EXTI2)
{
	__disable_interrupt();
//	if(shared_data.laser_en)
//	while(1)
	_serializedata(shared_data.line + (doswap ? 30 : 0), 200);
	GPIO_LASER->ODR |= (1 << PIN_LASER);
	__enable_interrupt();
}

PROTO_UART_INTERRUPT(rx);


int main()
{
	shared_data.reset_pending = 0;
	shared_data.set_mirror_freq_pending = 0;
	for(int i = 0; i < 200; i++){
		if(i%2){
		shared_data.line[i] = 0b11111111;
		}
		else
		{
		shared_data.line[i] = 0b00000000;
		}
	}
	__disable_interrupt();
	CLK_INIT(CLOCK_SOURCE_INTERNAL, 0); //should be external

	pinmode(PIN_CLK, GPIO_CLK, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0); //clock initialized
	pinmode(PIN_POLY, GPIO_POLY, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1); //polygon mirror S/S
	pinmode(PIN_LASER, GPIO_LASER, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1); //laser


	SetupTimer2();
	uart_init(921600);
	set_freq(300);
	TIM4_init();
	active_millis = millis();

	GPIO_POLY->ODR &= ~(1 << PIN_POLY); //pull down to enable polygon mirror
//memset(shared_data.line, 0b10101010, 100);
	pinmode(PIN_EOLD, GPIO_EOLD, INPUT, INPUT_CR1_PULLUP, INPUT_CR2_INTERRUPT, 0); //end of line detector
	pinmode(PIN_PLOCK, GPIO_PLOCK, INPUT, INPUT_CR1_PULLUP, INPUT_CR2_INTERRUPT, 0); //mirror locked
	EXTI->CR1 &= ~EXTI_CR1_PDIS;
	EXTI->CR1 &= ~EXTI_CR1_PCIS;
	EXTI->CR1 |= 0b10010000;

	_getchar = uart_getchar;
	_putchar = uart_putchar;

	shared_data.clk_delay = 200;


	__enable_interrupt();
	while (1)
	{

		if(shared_data.reset_pending){
			soft_reset();
		}

	if(shared_data.set_mirror_freq_pending){
		set_freq(shared_data.mirror_frequency);
		shared_data.set_mirror_freq_pending = 0;
	}

	/*if(TIMER_TICK_8(swap, 100)){
		doswap ^= 1;
		TIMER_REFRESH_8(swap);
	}*/


		PROTO_HOOK(rx);

		if(TIMER_TICK_8(active, shared_data.clk_delay)){
			GPIO_CLK->ODR ^= (1 << PIN_CLK); //running
			TIMER_REFRESH_8(active);
		}

	}
}
