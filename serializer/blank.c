#include <stm8s.h>
#include <common.h>
#include <serializer/serializer.h>
#include <millis/tim4millis.h>


void (*_serializedata)(uint8_t *data, uint8_t size) = SERIALIZER_ADDRESS;

//WARING: SERIALIZER HAS TO BE ALIGNED BY 4

__at(0x23) uint8_t data[] = {0b00100101, 0b01010101, 0b01010101};

int main() {
	CLK_INIT(CLOCK_SOURCE_INTERNAL, 0);
//	pinmode(3, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);
	pinmode(2, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1);
//	TIM4_init();
	//__disable_interrupt();
	__enable_interrupt();
	while(1){
//		data[0] ^= (1 << 7);
//		data[0] ^= (1 << 6);
//		data[0] ^= (1 << 5);
//		data[1] ^= (1 << 7);
//		data[1] ^= (1 << 7);
		_serializedata(data, sizeof(data));
	}
}
