//
//  This program shows how you can generate a 20 Hz signal using
//  Timer 2 and Port D, Pin 5 on the STM8S microcontroller.
//
//  This software is provided under the CC BY-SA 3.0 licence.  A
//  copy of this licence can be found at:
//
//  http://creativecommons.org/licenses/by-sa/3.0/legalcode
//
#include <stdint.h>
#include <stm8s.h>
#include "tim4millis.h"
#include <math.h>
#include <common.h>
#include <eeprom.h>

inline void laser_on(){
	*(uint8_t*)GPIOD_BaseAddress ^= (1 << 6);
}

inline void laser_off(){
	*(uint8_t*)GPIOD_BaseAddress  ^= ~(1 << 6);
}

void set_freq(uint16_t freq){
	uint16_t f_half;
	uint16_t f;
	uint8_t h;
	uint8_t l;
	uint8_t hh;
	uint8_t hl;
	uint32_t constant = 0xef9020; //product of frequency and
	f = constant/freq;
	f_half = f/2;
	h  = ((uint8_t*)(&f))[0];
	l  = ((uint8_t*)(&f))[1];
	hh = ((uint8_t*)(&f_half))[0];
	hl = ((uint8_t*)(&f_half))[1];
	TIM2->ARRH = h;       //  High byte of 50,000 . (freq)
	TIM2->ARRL = l;       //  Low byte of 50,000.
	TIM2->CCR3H = hh;
	TIM2->CCR3L = hl;
}

//

//
void SetupTimer2()
{
	TIM2->PSCR = TIM2_PRESCALER_1;       //  Prescaler = 8.
	TIM2->ARRH = 0x10;       //  High byte of 50,000 . (freq)
	TIM2->ARRL = 0x00;       //  Low byte of 50,000.
    TIM2->CCER2 |= TIM2_CCER2_CC3P;    //  Active high.
	TIM2->CCER2 |= TIM2_CCER2_CC3E;    //  Enable compare mode for channel 1.
    TIM2->CCMR3 |= TIM2_CCMR_OCM;    //  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
  //  TIM2->IER |= TIM2_IER_UIE;       //  Enable the update interrupts.
    TIM2->CR1 |= TIM2_CR1_CEN;       //  Finally enable the timer.
}

//
//  Main program loop.
//

#define AMPL 100

void mirror_locked() __interrupt(IRQ_EXTI3)
{
	__disable_interrupt();
	laser_on();
	__enable_interrupt();
}
volatile uint16_t stackpointer = 0xf1f0;
int tonmax = 30;
int toffmax = 40;
int tadd = 1;
int tfadd = 1;
int ton = 0;
int toff = 0;

void end_of_line() __interrupt(IRQ_EXTI2)
{
	int i = 0, u = 0;
	uint32_t o = 120000;
	uint8_t *addr = 50;
	__disable_interrupt();
	for(i = 0; i < 70; i++){
		uint8_t byte = *addr++;
		uint8_t ind = 0;
		for(u = 0; u < 7; u++){
		if(byte & (1 << ind++))
			*(uint8_t*)GPIOD_BaseAddress |= (1 << 6);
		else
			*(uint8_t*)GPIOD_BaseAddress &= ~(1 << 6);
		}
		o = 1;
		while(o--);
	}
	*(uint8_t*)GPIOD_BaseAddress |= (1 << 6);
	__enable_interrupt();
}

uint32_t active_millis = 0;
uint32_t dance_millis = 0;
int main()
{


    __disable_interrupt();
	CLK_INIT(CLOCK_SOURCE_EXTERNAL, 0);

	pinmode(5, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0); //clock initialized
	pinmode(5, GPIOB, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1); //polygon mirror S/S
	pinmode(6, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0); //laser


    SetupTimer2();
	set_freq(400);
	TIM4_init();
	active_millis = millis();

	GPIOB->ODR &= ~(1 << 5); //pull down to enable polygon mirror

	pinmode(4, GPIOC, INPUT, INPUT_CR1_PULLUP, INPUT_CR2_INTERRUPT, 0); //end of line detector
	pinmode(4, GPIOD, INPUT, INPUT_CR1_PULLUP, INPUT_CR2_INTERRUPT, 0); //mirror locked
	EXTI->CR1 &= ~EXTI_CR1_PDIS;
	EXTI->CR1 &= ~EXTI_CR1_PCIS;
	EXTI->CR1 |= 0b10010000;

    __enable_interrupt();
    while (1)
    {
		if(current_millis - active_millis > 100){
			GPIOD->ODR ^= (1 << 5); //running
			active_millis = current_millis;
		}

		if(current_millis - dance_millis > 30){
			ton += tadd;
			toff += tfadd;
			if(ton == tonmax)
				tadd = -1;
			else if (ton == 0)
				tadd = 1;

			if(toff == toffmax)
				tfadd = -1;
			else if (toff == 0)
				tfadd = 1;


			dance_millis = current_millis;
		}
	}
}
