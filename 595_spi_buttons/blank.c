#include <stm8s.h>
#include <millis/tim4millis.h>
#include <delay.h>
#include <common.h>
#include <spi595/spi_595.h>

uint8_t lastcounter;
uint8_t __at (0x0000) counter; //this preserves variables trought resets.

volatile uint8_t ilock;
volatile uint8_t debounce_millis;

void buttonhandler() __interrupt(6) //port D interrupt
{
	__disable_interrupt();
	if(!ilock){
		ilock = 1;
		debounce_millis = current_millis_uint8;
	}
	__enable_interrupt();
}

int main()
{
	spi595_t sp;
	__disable_interrupt();
	CLK_INIT(CLK->ICKR, 0);
	SPI_INIT_MASTER(SPI_BAUDRATEPRESCALER_2, SPI_CLOCKPOLARITY_LOW, SPI_CLOCKPHASE_1EDGE);
	TIM4_init();

	EXTI->CR1 |= EXTI_CR1_PDIS & 0b11000000; //full mask, interrupt on rising and falling edges

	//pinmode(3, GPIOA, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, LOW); // 595 latch
    pinmode(3, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, LOW); // 595 OE
    pinmode(2, GPIOD, INPUT, INPUT_CR1_PULLUP, INPUT_CR2_INTERRUPT, HIGH);

    spi595_create(&sp, GPIOA, 3);
	spi595_setbyte(&sp, counter);


    ilock = 0;

	__enable_interrupt();
    // Loop
    while(1){
        //debouncing
		__disable_interrupt();
		nope:
		if(ilock && (current_millis_uint8 - (uint16_t)debounce_millis > 20)){


			//button pushed down
			if(ilock == 1)
			{
				//debounced button is pushed
				if(!(GPIOD->IDR & (1 << 2))){
				//	counter++;
					ilock = 2;
				}
				else //debounced button was released during the debouncing period
				{
					ilock = 0;
					goto nope;
				}
			}

			if(GPIOD->IDR & (1 << 2)){
				counter++;
				ilock = 0;
			}
		}
		__enable_interrupt();

        if(lastcounter != counter){
            spi595_setbyte(&sp, counter);
            lastcounter = counter;
        }

    }
}
