#include <stm8s.h>
#include <stdlib.h>
#include <millis/tim4millis.h>
#include <common.h>
#include <math.h>

//this is was thought for a cat toy where a motor is spun shortly during randoms periods of time

#define DELAY_SET(name) uint16_t name
#define DELAY_HANDLER_BEGIN(name, delay) if(current_millis_uint16 - (uint32_t)(name) > (delay)){
#define DELAY_HANDLER_END(name) name = current_millis_uint16; }

DELAY_SET(delay);
DELAY_SET(motor);

uint16_t delays[] = {5, 200};
uint16_t motordelays[] = {10, 1000};

int main() {

	CLK_INIT(CLK->ICKR, 0);
	TIM4_init();

	pinmode(3, GPIOA, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);
	pinmode(3, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);

	__enable_interrupt();


	//main loop and shit
	while(1){

		//blink light to signal cpu running
		DELAY_HANDLER_BEGIN(delay, delays[!(GPIOA->ODR & (1 << 3))]){
			GPIOA->ODR ^= (1 << 3);
		}
		DELAY_HANDLER_END(delay)

		DELAY_HANDLER_BEGIN(motor, motordelays[!(GPIOD->ODR & (1 << 3))]){
			GPIOD->ODR ^= 1 << 3;
			motordelays[0] = 10 + rand() % 10;
			motordelays[1] = 1000 + rand() % 4000;

		}
		DELAY_HANDLER_END(motor)
	}

}
