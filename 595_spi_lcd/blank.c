#include <stm8s.h>
#include <millis/tim4millis.h>
#include <delay.h>
#include <common.h>
#include <spi595/spi_595.h>
#include <spi595/lcd.h>
#include <lcd/lcd_base.h>

uint32_t counter_millis = 0;
spi595_t s595;
spilcd_t lcd;

uint8_t smiley[8] = {
  0b00000,
  0b00000,
  0b00100,
  0b01000,
  0b10000,
  0b00001,
  0b00010,
  0b00100,
};


int x = 0;
int s = 12;
int increments_x = 1;
int increments_s = -1;

int main()
{
    uint8_t n = 0;
    uint8_t whoops = 0;
    uint8_t counter = 0;
    char buff[2];
    CLK_INIT(CLK->ICKR, 0);
    SPI_INIT_MASTER(SPI_BAUDRATEPRESCALER_8, SPI_CLOCKPOLARITY_LOW, SPI_CLOCKPHASE_1EDGE);
    TIM4_init();

    __enable_interrupt();
    spi595_create(&s595, GPIOA, 3);
    spi595_setbyte(&s595, 1 << 1); //turn on backlight

	lcd_create(&lcd.lcd, 0, 2, 3, 4, 5, 6, 7);

	lcd_createChar(&lcd.lcd, 1, smiley);
	lcd_begin(&lcd.lcd, 2, LCD_5x8DOTS);
    counter  = 0;

    //loop
    while(1){
        int i = 0;
       if(millis() - counter_millis > 500){
		 lcd_clear(&lcd.lcd);
            //put data to be sent into the spi bus
        (*buff)++;
        buff[1] = 0;

        n++;
		lcd_setCursor(&lcd.lcd, 0, 0);

        for(i = 0; i < x; i++)
			lcd_printf(&lcd.lcd, " ");
		lcd_printf(&lcd.lcd, "FUCK");

		 lcd_setCursor(&lcd.lcd, 0, 1);

         for(i = 0; i < s; i++)
			 lcd_printf(&lcd.lcd, " ");
		 lcd_printf(&lcd.lcd, "SHIT");

        x += increments_x;
        s += increments_s;

        if(x >= 12){
            increments_x = -1;
        } else if(x == 0){
            increments_x = 1;
        }

        if(s >= 12){
            increments_s = -1;
        } else if(s == 0){
            increments_s = 1;
        }

            counter_millis = millis();
        }
    }
}
