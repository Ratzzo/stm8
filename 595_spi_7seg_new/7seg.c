#include "7seg.h"

//
//   3
// 2   4
//   1
// 0   6
//   5   7

#define DIGIT(b0, b1, b2, b3, b4, b5, b6, b7)  ((uint8_t)(1 << b0) | (uint8_t)(1 << b1) | (uint8_t)(1 << b2) | (uint8_t)(1 << b3) | (uint8_t)(1 << b4) | (uint8_t)(1 << b5) | (uint8_t)(1 << b6) | (uint8_t)(1 << b7))


const uint8_t digits[] = {
	DIGIT(3, 2, 4, 5, 6, 0, 0, 0), //0
	DIGIT(4, 6, 6, 6, 6, 6, 6, 6), //1
	DIGIT(3, 4, 1, 0, 5, 5, 5, 5), //2
	DIGIT(3,4,1,6,5,5,5,5), //3
	DIGIT(4,2,1,6,6,6,6,6), //4
	DIGIT(3,2,1,6,5,5,5,5), //5
	DIGIT(3,2,1,0,5,6,6,6), //6
	DIGIT(3,4,6,6,6,6,6,6), //7
	DIGIT(3,2,4,1,0,6,5,5), //8
	DIGIT(2,3,4,1,6,5,5,5), //9
	0
};

void spi595_7seg_setnumber(spi595_t *s, uint8_t number, uint8_t dot){
	spi595_setbyte(s, ~(digits[(number % 11)] | (dot ? 1 << 7 : 0)));
}


