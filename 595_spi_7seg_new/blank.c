#include <stm8s.h>
#include <millis/tim4millis.h>
#include <delay.h>
#include <common.h>
#include <spi595/spi_595.h>
#include "7seg.h"

uint8_t lastcounter;
uint8_t counter;
uint8_t ilock;
uint32_t counter_millis;

#define BLINKER 5

int main()
{
//	uint8_t counter = 0;
	spi595_t sp;
	CLK_INIT(CLK->ICKR, 0);
	SPI_INIT_MASTER(SPI_BAUDRATEPRESCALER_2, SPI_CLOCKPOLARITY_LOW, SPI_CLOCKPHASE_1EDGE);
	TIM4_init();

//	EXTI->CR1 = 0;// |= EXTI_CR1_PDIS & 0b11000000; //full mask, interrupt on rising and falling edges

	//pinmode(3, GPIOA, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, LOW); // 595 OE
// 	pinmode(2, GPIOD, INPUT, INPUT_CR1_PULLUP, INPUT_CR2_NOINTERRUPT, HIGH);
	pinmode(BLINKER, GPIOB, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, LOW);
	counter = 0;
	spi595_create(&sp, GPIOA, 3);
	spi595_setbyte(&sp, counter);


	counter_millis = 1000;

	__enable_interrupt();
	// Loop
	while(1){
		__enable_interrupt();
		if(millis() - counter_millis > 100){
			spi595_7seg_setnumber(&sp, counter, counter % 3);
//			spi595_setbyte(&sp, 1 << (counter % 9));
			counter++;
			GPIOB->ODR ^= (1 << BLINKER);
			counter_millis = millis();
		}
	}
}
