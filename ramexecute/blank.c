#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stm8s.h>
#include <millis/tim4millis.h>
#include <common.h>
#include <math.h>
#include <uart.h>
#include <string.h>



DEFINE_TIMER_16(delay);
DEFINE_TIMER_16(delay2);
uint8_t direction = 0;
uint16_t counter = 0;


typedef int (*blinky_t)();

int localfunction(int i){
	return i*10;
}

//the function to be stored
int blinky(){
	if(TIMER_TICK_16(delay2, 2000)){
		GPIOD->ODR ^= (1 << 3);
		printf("hello from ram :D %i\n", localfunction(12));
		TIMER_REFRESH_16(delay2);
	}
	return 0;
}

int dumpmem(char *mem, int size){
	int i = 0;
	while(size--){

		printf("%2.X ", mem[i]);
		i++;
		if(!(i % 8)){
			printf("\n");
		}
	}
	printf("\n");
	return 0;
}

int main() {
	int size = (uint16_t)dumpmem - (uint16_t)blinky; //determine function code size
	char *m = malloc(size);
	CLK_INIT(CLK->ICKR, 0);
	TIM4_init();
	uart_init(921600);
	UART1->CR2 &= ~(UART1_CR2_RIEN); //disable uart receive interrupt
	memcpy(m, blinky, size); //copy function code to malloced space


	pinmode(3, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);

	__enable_interrupt();

	_putchar = uart_putchar;
	_getchar = uart_getchar;


	//main loop and shit
	while(1){
		if(TIMER_TICK_16(delay, 1000)){

	//		dumpmem((uint16_t)blinky, size);
	//		dumpmem(m, size);
	//		printf("malloc %x\n", );
			TIMER_REFRESH_16(delay);
		}
		((blinky_t)m)(); //execute memory :D
	}

}
