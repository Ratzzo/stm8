#include <stm8s.h>
#include <millis/tim4millis.h>
#include <delay.h>
#include <common.h>
#include <spi595/spi_595.h>
#include <pcf/pcf8574.h>
#include <pcf/buttons.h>
#include <eeprom.h>
#include <twi/twi.h>

uint8_t last_out_595;
volatile uint8_t out_595;
uint8_t address = 0;
uint32_t counter_millis;

volatile uint8_t ilock;
spi595_t sp;


void interrupt_handler() __interrupt(IRQ_EXTI3)
{
	pcfbuttons_handle_interrupt();
}

uint8_t button_handler(uint8_t state, uint8_t button){
	//address++
	if(button == 0 && state == 1){
		address++;
		eeprom_read(address, &out_595, 1);
		}
	//address--
	if(button == 1 && state == 1){
		address--;
		eeprom_read(address, &out_595, 1);
		}
	//data++
	if(button == 2 && state == 1){
		out_595++;
		eeprom_write(address, &out_595, 1);
	}
	//data--
	if(button == 3 && state == 1){
		out_595--;
		eeprom_write(address, &out_595, 1);
	}


//	counter ^= (state << (7-button));

	return 0;
}

int main()
{
	pcf8574_t pcf;
	__disable_interrupt();
	CLK_INIT(CLK->ICKR, 0);
	SPI_INIT_MASTER(SPI_BAUDRATEPRESCALER_2, SPI_CLOCKPOLARITY_LOW, SPI_CLOCKPHASE_1EDGE);
	TIM4_init();
	IWDG_INIT(IWDG_Prescaler_128, 0xff);

	//EXTI->CR1 |= 0b10000000;EXTI_CR1_PDIS; //full mask, interrupt on rising and falling edges
	//GPIOD->ODR &= ~(1 << 2);
	twi_init();

	//pinmode(3, GPIOA, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, LOW); // 595 latch
	pinmode(2, GPIOD, OUTPUT, OUTPUT_CR1_OPENDRAIN, OUTPUT_CR2_10MHZ, HIGH); // 595 OE
	pinmode(3, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, HIGH); // 595 OE



    spi595_create(&sp, GPIOA, 3);
	eeprom_read(address, &out_595, 1);
	spi595_setbyte(&sp, out_595);

	__enable_interrupt();

	//turn pnp transistor that enables peripherals
	GPIOD->ODR &= ~(1 << 3);


	pcf8574_create(&pcf, 0);
	pcfbuttons_create(&pcf, 0xff, button_handler, 4, GPIOD);

    ilock = 0;

    // Loop
    while(1){
		if(last_out_595 != out_595){
			spi595_setbyte(&sp, out_595);
			last_out_595 = out_595;
		}

		pcfbuttons_listen();

	IWDG_FEED();
    }
}
