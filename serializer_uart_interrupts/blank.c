#include <stm8s.h>
#include <common.h>
#include <serializer/serializer.h>
#include <millis/tim4millis.h>
#include <uart.h>
#include <ring_buffer.h>

void (*_serializedata)(uint8_t *data, uint8_t size) = SERIALIZER_ADDRESS;
#define RX_BUFSIZE 16

DEFINE_RING(rx, RX_BUFSIZE);

uint8_t data[] = {0b01010101, 0b01010101, 0b01010101};

void uart_charhandler() __interrupt(18) {
//	printf("%x ", data[1] = uart_getchar());
	__disable_interrupt();
	RING_PUT(rx, uart_getchar());
	__enable_interrupt();
}

int main() {
	CLK_INIT(CLOCK_SOURCE_INTERNAL, 0);
	uart_init(115200);
	pinmode(3, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1);
	pinmode(2, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1);
//	TIM4_init();


	_putchar = uart_putchar;
	_getchar = uart_getchar;

	__enable_interrupt();
	while(1){
		int32_t m = 4;
		while(m--){

		}


		//__disable_interrupt();
		while(RING_NOTEMPTY(rx)){
			uint8_t current_char;
			RING_GET(rx, current_char);
			uart_putchar(current_char);
		}
		//__enable_interrupt();

		_serializedata(data, sizeof(data));

	}
}
