#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stm8s.h>
#include <millis/tim4millis.h>
#include <common.h>
#include <math.h>
#include <uart.h>

DEFINE_TIMER_16(delay);
uint8_t direction = 0;
uint16_t counter = 0;

void uart_charhandler() __interrupt(IRQ_UART1_RDF) {
	printf("%x ", uart_getchar());
}

void timer1_update() __interrupt(IRQ_TIM1_UOUTB){
//	printf("overflow\n");
	TIM1->SR1 = (uint8_t)(~TIM1_IT_UPDATE);
}

int main() {
	CLK_INIT(CLK->ICKR, 0);
	TIM4_init();
	uart_init(921600);

	pinmode(3, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);

	__enable_interrupt();

	TIM1->CR2 = 0x00;
	TIM1->SMCR = 0x00;
	TIM1->ETR = 0x00;
	TIM1->ARRH = 0xFF; //ARR is the upper limit of the counter, after it reaches this value, it overflows
	TIM1->ARRL = 0xFF;
	TIM1->PSCRH = 0x00; //no prescaling, count at full cpu.
	TIM1->PSCRL = 0x00;
	TIM1->IER = TIM1_IER_UIE; //update interrupt

	TIM1->CR1 = TIM1_CR1_CEN;

	_putchar = uart_putchar;
	_getchar = uart_getchar;


	//main loop and shit
	while(1){
		uint16_t t_i = 0; //initial time
		uint16_t t_f = 0; //final time
		if(TIMER_TICK_16(delay, 100)){
			GPIOD->ODR ^= (1 << 3);


			t_i = (TIM1->CNTRH << 8) | TIM1->CNTRL;
			{
				__asm
				nop
				nop //keep adding nops
				__endasm;
			}
			t_f = (TIM1->CNTRH << 8) | TIM1->CNTRL;

			printf("block executed in %i cpu cycles\n", t_f-t_i-12); //12 counts takes to store the variables.
			TIMER_REFRESH_16(delay);
		}
	}

}
