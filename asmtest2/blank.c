#include <stm8s.h>
#include <common.h>

__at(0x00) uint8_t byte;

uint8_t serialize_data(uint8_t *data, uint8_t size){
//	bres	0x500F, #2
	uint8_t index = 0;
	while(size--){
		for(uint8_t i = 0; i < 8; i++)
			if(*data & (1 << i)){
				__asm
				bset	0x500F, #2
				__endasm;
				}
				else
				{
				__asm
				bres	0x500F, #2
				__endasm;
				}
		data++;
	}
}

int main() {
	CLK_INIT(CLOCK_SOURCE_INTERNAL, 0);
//	pinmode(3, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);
	pinmode(2, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1);
	byte = 0b10101010;
	while(1)
	serialize_data(0x0000, 1);

	return 0;
}
