#include <stm8s.h>
#include <common.h>

__at(0x11) uint8_t byte;
__at(0xff) uint8_t count;

int main() {
	CLK_INIT(CLOCK_SOURCE_INTERNAL, 0);
//	pinmode(3, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);
	pinmode(2, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1);
	__disable_interrupt();
__asm;
	clrw x
	_exit:
	mov 0x0000, #0b01010101
	mov 0x0001, #0b01010101
	mov 0x0002, #0b01010101
	mov 0x0000, #0b01011010
	mov 0x0001, #0b01010101
	mov 0x0002, #0b10101010
	ldw x, #0x4000
	ldw y, #_count
	mov _count, #0xff
	inc (y)
	back$: //this is not very stable timing wise, more nops should be added, and I'm too lazy to implement it that way
	dec (y)
	jreq _exit
	ld a, (x)
	incw x
	ld _byte, a

	nop
	btjf _byte, #0, set0
	bres 0x500F, #2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	jrt noset0
	set0:
	bset 0x500F, #2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	//	  nop
	nop
	nop
	nop
	noset0:

	btjf _byte, #1, set1
	bres 0x500F, #2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	jrt noset1
	set1:
	bset 0x500F, #2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	noset1:

	btjf _byte, #2, set2
	bres 0x500F, #2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	jrt noset2
	set2:
	bset 0x500F, #2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	noset2:

	btjf _byte, #3, set3
	bres 0x500F, #2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	jrt noset3
	set3:
	bset 0x500F, #2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	noset3:

	btjf _byte, #4, set4
	bres 0x500F, #2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	jrt noset4
	set4:
	bset 0x500F, #2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop

	noset4:

	btjf _byte, #5, set5
	bres 0x500F, #2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	jrt noset5
	set5:
	bset 0x500F, #2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop

	noset5:

	btjf _byte, #6, set6
	bres 0x500F, #2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	jrt noset6
	set6:
	bset 0x500F, #2
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	noset6:

	btjf _byte, #7, set7
	bres 0x500F, #2
	nop
	nop


	jrt noset7
	set7:
	bset 0x500F, #2
	nop
	nop
	nop
	nop
	noset7:
	jp back$
	__endasm;
	return 0;
}
