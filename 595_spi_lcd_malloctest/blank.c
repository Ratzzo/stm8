#include <stm8s.h>
#include <millis/tim4millis.h>
#include <delay.h>
#include <common.h>
#include <lcd/lcd_base.h>
#include <spi595/spi_595.h>
#include <spi595/lcd.h>
#include <stdlib.h>
#include <string.h>

uint32_t counter_millis = 0;
spi595_t s595;
spilcd_t lcd;



int x = 0;
int s = 12;
int increments_x = 1;
int increments_s = -1;

int main()
{
    uint8_t n = 0;
    uint8_t *alloc;
    uint8_t counter = 0;
    CLK_INIT(CLK->ICKR, 0);
    SPI_INIT_MASTER(SPI_BAUDRATEPRESCALER_8, SPI_CLOCKPOLARITY_LOW, SPI_CLOCKPHASE_1EDGE);
    TIM4_init();

    alloc = malloc(200);
	memset(alloc, 0xff, 200);

    __enable_interrupt();
    spi595_create(&s595, GPIOA, 3);
    spi595_setbyte(&s595, 1 << 1); //turn on backlight

	spilcd_create(&lcd, &s595, 2, 3, 4, 5, 6, 7);

   // lcd_createChar(&lcd, 1, smiley);
	lcd_begin(&lcd.lcd, 2, LCD_5x8DOTS);
	lcd_printf(&lcd.lcd, "%.4x", alloc);
	lcd_setCursor(&lcd.lcd, 0, 1);
	lcd_printf(&lcd.lcd, "%.4x", malloc(200));
    counter  = 0;

    //loop
    while(1){

    }
}
