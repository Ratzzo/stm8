#include <stm8s.h>
#include <common.h>
#include <millis/tim4millis.h>
#include <packedintarray/packedintarray.h>

DEFINE_TIMER_8(active);

packedintarray_t p;
__at(0x130) uint8_t *location;

typedef struct pin_t {
	GPIO_TypeDef *GPIO;
	uint8_t pin;
} pin_t;

const pin_t pins[] = {
	{GPIOD, 2},
//	{GPIOD, 1}, //cant use SWIM as GPIO with the header connected
	{GPIOC, 7},
	{GPIOC, 6},
	{GPIOC, 5},
	{GPIOC, 4},
	{GPIOC, 3},
	{GPIOB, 4},
	{GPIOB, 5},
	{GPIOA, 3},
	{GPIOA, 2},
	{GPIOA, 1},
	{GPIOD, 6},
	{GPIOD, 5},
	{GPIOD, 4},
};

void _interrupt_driven(){ //blinker
	if(TIMER_TICK_8(active, 100)){
		GPIOD->ODR ^= (1 << 3);
		TIMER_REFRESH_8(active);
	}
}

int cindex; //current index
int turn_on_next(){
	pins[cindex].GPIO->ODR &= ~(1 << pins[cindex].pin);
	cindex++;
	if(cindex >= sizeof(pins)/sizeof(pin_t))
		cindex = 0;
	return pins[cindex].pin;
}

int main() {
	uint8_t consistent = 1;
	CLK_INIT(CLOCK_SOURCE_INTERNAL, 0);
	TIM4_init();
	_tim4_inthandler = _interrupt_driven;

	__enable_interrupt();

	cindex = 0;
	reset:;
	for(int i = 0; i < sizeof(pins)/sizeof(pin_t);i++){
			pinmode(pins[i].pin, pins[i].GPIO, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1);
	}

	pinmode(3, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);


	//GPIOD->ODR &= ~(1 << 2);
	for(int o = 0; o < sizeof(pins)/sizeof(pin_t);o++){
		int bits = o*2+1;
		for(uint32_t m = 0; m < 100; m++){
			packedintarray_create(&p, bits, (200)/bits);
			for(uint32_t i = 0; i < p.length; i++){
				packedintarray_set(&p, i, i);
				if(!(packedintarray_get(&p, i) == i%((uint32_t)1 << p.intbits)))
					consistent = 0;
			}
			packedintarray_destroy(&p);
		}
		if(consistent)
			turn_on_next();
		consistent = 1;
	}
	goto reset;


}
