#include <stm8s.h>
#include <millis/tim4millis.h>
#include <delay.h>
#include <common.h>
#include <spi595/spi_595.h>

/** A3 595_LATCH
 *  C3 !595_ OE
 * `D4 running LED
 **/



uint8_t lastcounter;

uint8_t ilock;
uint32_t counter_millis;


int main()
{
//	uint8_t counter = 0;
	spi595_t sp;
	CLK_INIT(CLK->ICKR, 0);
	SPI_INIT_MASTER(SPI_BAUDRATEPRESCALER_8, SPI_CLOCKPOLARITY_LOW, SPI_CLOCKPHASE_1EDGE);
	TIM4_init();

//	EXTI->CR1 = 0;// |= EXTI_CR1_PDIS & 0b11000000; //full mask, interrupt on rising and falling edges

	pinmode(3, GPIOC, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, HIGH); //OE
	pinmode(4, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, HIGH);
	spi595_create(&sp, GPIOA, 3);
	spi595_setbyte(&sp, 0b01111011);

	GPIOC->ODR &= ~(1 << 3);

	counter_millis = 0;

	__enable_interrupt();
	// Loop
	while(1){
		__enable_interrupt();
		if(millis() - counter_millis > 1000){
			GPIOD->ODR ^= 1 << 4;
//			spi595_setbyte(&sp, 0b01111011);
//			spi595_setbyte(&sp, counter);

			counter_millis = millis();
		}
	}
}
