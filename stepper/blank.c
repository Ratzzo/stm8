#include <stm8s.h>
#include <millis/tim4millis.h>
#include <delay.h>
#include <common.h>
#include <spi595/spi_595.h>

#define LED 4

typedef struct step_t {
	GPIO_TypeDef *gpio;
	uint8_t pin;
} step_t;

step_t steps[] = {
	{GPIOD, 3},
	{GPIOC, 3},
	{GPIOD, 2},
	{GPIOC, 4},
};
step_t *current_step;
uint8_t stepindex = 0;

uint8_t lastcounter;

uint8_t ilock;
uint32_t counter_millis;
uint32_t step_millis;



int main()
{
	CLK_INIT(CLK->ICKR, 0);
	TIM4_init();

	pinmode(LED, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, LOW);

	for(int i = 0; i < sizeof(steps)/sizeof(step_t); i++){
		step_t *step = &steps[i];
		pinmode(step->pin, step->gpio, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, LOW);
	}

	counter_millis = 0;

	stepindex = 0;
	current_step = &steps[0];

	__enable_interrupt();
	// Loop
	while(1){
		__enable_interrupt();
		if(millis() - counter_millis > 1000){
			GPIOD->ODR ^= 1 << LED;

			counter_millis = millis();
		}

		if(millis() - step_millis > 100){
			current_step->gpio->ODR &= ~(1 << current_step->pin);
			current_step = &steps[stepindex++];
			current_step->gpio->ODR |= (1 << current_step->pin);
			if(stepindex == sizeof(steps)/sizeof(step_t)){
				stepindex = 0;
			}
			step_millis = millis();
		}
	}
}
