#include <stm8s.h>
#include <millis/tim4millis.h>
#include <delay.h>
#include <common.h>
#include <spi595/spi_595.h>

/** A3 595_LATCH
 *  D3 !595_ OE
 * `D4 running LED
 **/



uint8_t lastcounter;

uint8_t ilock;
uint32_t counter_millis;

uint32_t step_millis;
uint32_t stepindex;
uint8_t steps[] = {
	0b00001001,
	0b00000000,
	0b00000110,
	0b00000000,
};


int main()
{
//	uint8_t counter = 0;
	spi595_t sp;
	CLK_INIT(CLK->ICKR, 0);
	SPI_INIT_MASTER(SPI_BAUDRATEPRESCALER_8, SPI_CLOCKPOLARITY_LOW, SPI_CLOCKPHASE_1EDGE);
	TIM4_init();

//	EXTI->CR1 = 0;// |= EXTI_CR1_PDIS & 0b11000000; //full mask, interrupt on rising and falling edges

	pinmode(3, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, HIGH); //OE
	pinmode(4, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, HIGH);
	spi595_create(&sp, GPIOA, 3);
	spi595_setbyte(&sp, 0b00000000); //clears

	GPIOD->ODR &= ~(1 << 3); //enable outputs

	counter_millis = 0;
	step_millis = 1000;
	stepindex = 0;

	__enable_interrupt();
	// Loop
	while(1){
		__enable_interrupt();
		if(millis() - counter_millis > 100){
			GPIOD->ODR ^= 1 << 4;
			counter_millis = millis();
		}

		if(millis() - step_millis > 100){
			spi595_setbyte(&sp, steps[stepindex++]); //clears
			if(stepindex == sizeof(steps)/sizeof(uint8_t)){
				stepindex = 0;
			}
			step_millis = millis();
		}

	}
}
