#include <stm8s.h>
#include <millis/tim4millis.h>
#include <delay.h>
#include <common.h>
#include <lcd/lcd_base.h>
#include <spi595/spi_595.h>
#include <spi595/lcd.h>
#include <math.h>

uint32_t counter_millis = 0;
spi595_t s595;
spilcd_t lcd;

uint8_t smiley[8] = {
  0b00000,
  0b00000,
  0b00100,
  0b01000,
  0b10000,
  0b00001,
  0b00010,
  0b00100,
  };


  int x = 0;
  int s = 12;
  int increments_x = 1;


  volatile uint16_t adcdata;

  uint16_t averaging_buffer[10];
  uint16_t averaging_buffer_index;



  void adc1_irq() __interrupt (22){
      int avg_buf_len = sizeof(averaging_buffer)/sizeof(uint16_t);
      __disable_interrupt();
      ADC1->CR1 &= ~ADC1_CR1_ADON;
      ADC1->CSR &= ~ADC1_CSR_EOC;

      GPIOD->ODR ^=  (1 << 2);

      averaging_buffer[averaging_buffer_index] = *((uint16_t*)&ADC1->DRH);
      averaging_buffer_index++;

      if(averaging_buffer_index == avg_buf_len){
          int i;
          uint32_t result = adcdata;
          averaging_buffer_index = 0;

          for(i = 0; i < avg_buf_len; i++){
              result += averaging_buffer[i];
              result /= 2;
          }


        adcdata = result;

    }
    __enable_interrupt();
}

int init_adc(){
    ADC1->CR1 |= ADC1_CR1_ADON; //turn on the ADC
    ADC1->CR1 |= (ADC1_CR1_SPSEL & ADC1_PRESSEL_FCPU_D2); //prescaler
    ADC1->CSR |= (ADC1_CSR_CH & ADC1_CHANNEL_4); //use AIN4
    ADC1->CR3 &= ~(ADC1_CR3_DBUF); //disable data buffer
    ADC1->CR2 |= ADC1_CR2_ALIGN; //align data in 0 to 1023
    ADC1->CSR |= ADC1_CSR_EOCIE; //END OF CONVERSION INTERRUPT ENABLE
	return 0;
}

int main()
{
    uint8_t n = 0;
    uint8_t whoops = 0;
    uint8_t counter = 0;
    char buff[2];
    CLK_INIT(CLK->ICKR, 0);
    SPI_INIT_MASTER(SPI_BAUDRATEPRESCALER_2, SPI_CLOCKPOLARITY_LOW, SPI_CLOCKPHASE_1EDGE);
    TIM4_init();
    init_adc();


    GPIOD->DDR |=  (1 << 2); //out
    GPIOD->CR1 |=  (1 << 2); //pushpull
    GPIOD->CR2 |=  (1 << 2); //10mhz



    __enable_interrupt();
    spi595_create(&s595, GPIOA, 3);
    spi595_setbyte(&s595, 1 << 1); //turn on backlight

	spilcd_create(&lcd, &s595, 2, 3, 4, 5, 6, 7);

	lcd_createChar(&lcd.lcd, 1, smiley);
	lcd_begin(&lcd.lcd, 2, LCD_5x8DOTS);
    counter  = 0;

    //loop
    while(1){
        int i = 0;
        ADC1->CR1 |= ADC1_CR1_ADON; //turn on the ADC

       if(millis() - counter_millis > 100){
		 lcd_clear(&lcd.lcd);
            //put data to be sent into the spi bus
        (*buff)++;
        buff[1] = 0;

        n++;
		lcd_setCursor(&lcd.lcd, 0, 0);

      //  for(i = 0; i < x; i++)
	 //       lcd_printf(&lcd.lcd, " ");
		lcd_printf(&lcd.lcd, "sarcasmo:");

		 lcd_setCursor(&lcd.lcd, 0, 1);
		 lcd_printf(&lcd.lcd, "%u", adcdata/2);

        x += increments_x;

        if(x >= 12){
            increments_x = -1;
        } else if(x == 0){
            increments_x = 1;
        }

            counter_millis = millis();
        }
    }
}
