//
//  This program shows how you can generate a 20 Hz signal using
//  Timer 2 and Port D, Pin 5 on the STM8S microcontroller.
//
//  This software is provided under the CC BY-SA 3.0 licence.  A
//  copy of this licence can be found at:
//
//  http://creativecommons.org/licenses/by-sa/3.0/legalcode
//
#include <stdint.h>
#include <stm8s.h>
#include "tim4millis.h"
#include <math.h>
#include <common.h>
#include <eeprom.h>


void set_freq(uint32_t freq){
	uint16_t f_half;
	uint16_t f;
	uint8_t h;
	uint8_t l;
	uint8_t hh;
	uint8_t hl;
	uint32_t constant = 0xef9020; //product of frequency and
	f = constant/freq;
	f_half = f/2;
	h  = ((uint8_t*)(&f))[0];
	l  = ((uint8_t*)(&f))[1];
	hh = ((uint8_t*)(&f_half))[0];
	hl = ((uint8_t*)(&f_half))[1];
	TIM2->ARRH = h;       //  High byte of 50,000 . (freq)
	TIM2->ARRL = l;       //  Low byte of 50,000.
	TIM2->CCR1H = hh;
	TIM2->CCR1L = hl;
}

void SetupTimer2()
{
	TIM2->PSCR = TIM2_PRESCALER_1;
	TIM2->ARRH = 0x10;					//  High byte of 50,000 . (freq)
	TIM2->ARRL = 0x00;					//  Low byte of 50,000.
	TIM2->CCER1 |= TIM2_CCER1_CC1P;		//  Active high.
	TIM2->CCER1 |= TIM2_CCER1_CC1E;
	TIM2->CCMR1 |= TIM2_CCMR_OCM;		//  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
//	TIM2->IER &= ~TIM2_IER_UIE;		//  Enable the update interrupts.
	TIM2->CR1 |= TIM2_CR1_CEN;			//  Finally enable the timer.
}

int main()
{


    __disable_interrupt();
	CLK_INIT(CLOCK_SOURCE_INTERNAL, 0);

	pinmode(5, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0); //clock initialized
	pinmode(5, GPIOB, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 1); //polygon mirror S/S
	pinmode(6, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0); //laser


    SetupTimer2();
	set_freq(1000000);
	TIM4_init();


    __enable_interrupt();
    while (1)
    {

	}
}
