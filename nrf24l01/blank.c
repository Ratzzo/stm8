#include <stm8s.h>
#include <stm8s_spi.h>
#include <pcf/pcf8574.h>
#include <pcf/7seg.h>
#include <rf24/RF24.h>
#include <millis/tim4millis.h>
#include <common.h>
#include <twi/twi.h>

#define DATA_TYPE_STICK_ANALOG ((uint8_t)0x01)
#define DATA_TYPE_BUTTON       ((uint8_t)0x02)

uint8_t baseaddr[5] = {0x01, 0xF0, 0xF0, 0xF0, 0xF0};


uint32_t led_millis;
uint32_t lastmillis;
uint8_t initialized = 0;

uint16_t lastbuttondata = 0;
uint32_t laststickdata = 0;


int main() {
    rf24_t rf;
    uint8_t rf_val;
    CLK_INIT(CLK->ICKR, 0);
    SPI_INIT_MASTER(SPI_BAUDRATEPRESCALER_2, SPI_CLOCKPOLARITY_LOW, SPI_CLOCKPHASE_1EDGE);

    TIM4_init();
    led_millis = millis();
    lastmillis = millis();

	GPIOD->DDR |= (1 << 3);
	GPIOD->CR1 |= (1 << 3);
	GPIOD->CR2 |= (1 << 3);

    GPIOB->DDR |= (1 << 5);
	GPIOB->CR1 |= (1 << 5);
	GPIOB->CR2 |= (1 << 5);
	GPIOB->ODR |= (1 << 5);


    rf24_create(&rf, pinport(GPIOD, 5), pinport(GPIOD, 4));

    rf_val = rf24_begin(&rf);

    GPIOD->ODR |= (1 << 3);


    if(!rf_val){
        rf24_setRetries(&rf, 15,15);
        rf24_setPayloadSize(&rf, 8);
        rf24_openReadingPipe(&rf, 1, baseaddr);
        rf24_startListening(&rf);
        initialized = 1;
        GPIOB->ODR &= ~(1 << 5);

    }
	__enable_interrupt();
	// Loop
	while(1) {
		//for(d = 0; d < 29000; d++) { }

    if(initialized){


        //blink led to indicate data received
        if(millis() - lastmillis >  10){
            GPIOD->ODR &= ~(1 << 3);
        }

        //GPIOD->ODR &= ~(1 << 3);

        if(rf24_available(&rf, 0)){
            uint8_t buffer[8];
            uint8_t *bufferptr = buffer;

            uint16_t buttondata = 0;
            uint8_t done = 0;
            while(!done){

            done = rf24_read(&rf, buffer, sizeof(buffer));
           // GPIOD->ODR &= ~(1 << 3);
            //parse buffer
            switch(*(bufferptr++)){
                case DATA_TYPE_BUTTON:
                {
                    buttondata = *((uint16_t*)bufferptr);
                    if(lastbuttondata != buttondata){
                        //buttons changed
                        GPIOD->ODR |= (1 << 3);
                        lastbuttondata = buttondata;
                        lastmillis = millis();


                        //parse button pressed
                        /*
                        if(!(buttondata & PSB_PAD_DOWN)){
                            PORTC |= _BV(PC0);
                        } else {
                            PORTC &= ~_BV(PC0);
                        }
                        if(!(buttondata & PSB_PAD_UP)){
                            PORTC |= _BV(PC1);
                        } else {
                            PORTC &= ~_BV(PC1);
                        } */
                    }
                    break;
                case DATA_TYPE_STICK_ANALOG:
                {
                    uint32_t stickdata = *((uint32_t*)bufferptr);
                    if(laststickdata != stickdata){
                        GPIOD->ODR |= (1 << 3);
                        laststickdata = stickdata;
                        lastmillis = millis();


                        //parse stick data

                    }
                }
                break;
                }
            }


            }
        }
	}


	};

}
