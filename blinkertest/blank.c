#include <stm8s.h>
#include <common.h>
#include <millis/tim4millis.h>

typedef struct pin{
	uint8_t number;
	uint16_t interval;
	uint16_t lastmillis;
	GPIO_TypeDef *gpio;
} pin;

pin pins[] = {
	{3, 100 +  20, 0, GPIOD},
	{1, 100 +  50, 0, GPIOD},
	{7, 100 +  70, 0, GPIOC},
	{6, 100 + 110, 0, GPIOC},
	{5, 100 + 130, 0, GPIOC},
	{4, 100 + 170, 0, GPIOC},
	{3, 100 + 190, 0, GPIOC},
	{4, 100 + 230, 0, GPIOB},
	{5, 100 + 290, 0, GPIOB},
	{3, 100 + 310, 0, GPIOA},
	{2, 100 + 370, 0, GPIOA},
	{1, 100 + 410, 0, GPIOA},
	{6, 100 + 430, 0, GPIOD},
	{5, 100 + 470, 0, GPIOD},
	{4, 100 + 530, 0, GPIOD},
};



int main() {
	int i;
	CLK_INIT(CLOCK_SOURCE_INTERNAL, 0);
	TIM4_init();

	for(i=0;i < sizeof(pins)/sizeof(pin);i++){
		pinmode(pins[i].number, pins[i].gpio, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);
	}

	__enable_interrupt();
	while(1){
		for(i=0;i < sizeof(pins)/sizeof(pin);i++)
			if(current_millis_uint16 - (uint32_t)pins[i].interval > pins[i].lastmillis){
				pins[i].gpio->ODR ^= (1 << pins[i].number);
				pins[i].lastmillis = current_millis_uint16;
			}
	}
}
