#include <stm8s.h>
#include <millis/tim4millis.h>
#include <delay.h>
#include <common.h>
#include <spi595/spi_595.h>

uint8_t lastcounter;
uint8_t counter;
uint8_t ilock;
uint32_t counter_millis;

#define DIGIT(b0, b1, b2, b3, b4, b5, b6, b7)  ((uint8_t)(1 << b0) | (uint8_t)(1 << b1) | (uint8_t)(1 << b2) | (uint8_t)(1 << b3) | (uint8_t)(1 << b4) | (uint8_t)(1 << b5) | (uint8_t)(1 << b6) | (uint8_t)(1 << b7))

const uint8_t digs[] = {
	DIGIT(5, 6, 4, 3, 2, 0, 0, 0), //0
	DIGIT(0, 2, 2, 2, 2, 2, 2, 2), //1
	DIGIT(5, 0, 7, 4, 3, 3, 3, 3), //2
	DIGIT(5, 0, 7, 2, 3, 3, 3, 3), //3
	DIGIT(0, 6, 7, 2, 2, 2, 2, 2), //4
	DIGIT(5, 6, 7, 2, 3, 3, 3, 3), //5
	DIGIT(5, 6, 7, 4, 3, 2, 2, 2), //6
	DIGIT(5, 0, 2, 2, 2, 2, 2, 2), //7
	DIGIT(5, 0, 6, 7, 4, 2, 3, 3), //8
	DIGIT(5, 0, 6, 7, 2, 3, 3, 3), //9
	0
};

#define BLINKER 5

int main()
{
//	uint8_t counter = 0;
	spi595_t sp;
	CLK_INIT(CLK->ICKR, 0);
	SPI_INIT_MASTER(SPI_BAUDRATEPRESCALER_2, SPI_CLOCKPOLARITY_LOW, SPI_CLOCKPHASE_1EDGE);
	TIM4_init();

//	EXTI->CR1 = 0;// |= EXTI_CR1_PDIS & 0b11000000; //full mask, interrupt on rising and falling edges

	//pinmode(3, GPIOA, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, LOW); // 595 OE
// 	pinmode(2, GPIOD, INPUT, INPUT_CR1_PULLUP, INPUT_CR2_NOINTERRUPT, HIGH);
	pinmode(BLINKER, GPIOB, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, LOW);
	pinmode(3, GPIOD, OUTPUT, OUTPUT_CR1_PUSHPULL, OUTPUT_CR2_10MHZ, 0);
	counter = 0;
	spi595_create(&sp, GPIOA, 3);
//	spi595_setbyte(&sp, counter);


	counter_millis = 0;

	__enable_interrupt();
	// Loop
	while(1){
		__enable_interrupt();
		if(millis() - counter_millis > 500){
			//spi595_7seg_setnumber(&sp, counter, counter % 3);
			spi595_writebyte(&sp, ~(digs[(counter % 11)]));
			spi595_writebyte(&sp, ~(digs[(counter % 11)]));
			spi595_writebyte(&sp, ~(digs[(counter % 11)]));
			spi595_writebyte(&sp, ~(digs[(counter % 11)]));
			spi595_writebyte(&sp, ~(digs[(counter % 11)]));
			spi595_writebyte(&sp, ~(digs[(counter % 11)]));
			spi595_writebyte(&sp, ~(digs[(counter % 11)]));
//			spi595_writebyte(&sp, (1 << counter));
			spi595_latch(&sp);
			counter++;
			if(counter == 11) counter = 0;
			GPIOB->ODR ^= (1 << BLINKER);
			counter_millis = millis();
		}
	}
}
