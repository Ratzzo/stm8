#include <stm8s.h>	
#include <millis/tim4millis.h>
#include <common.h>

DEFINE_TIMER(inc);

void init_sysclock()
{
	CLK->ICKR = 0;							//  Reset the Internal Clock Register.
	CLK->ICKR |= CLK_ICKR_HSIEN;			//  Enable the HSI.
	CLK->ECKR = 0;							//  Disable the external clock.
	while (!(CLK->ICKR & CLK_ICKR_HSIRDY));	//  Wait for the HSI to be ready for use.
	CLK->CKDIVR = 0;						//  Ensure the clocks are running at full speed.
	CLK->PCKENR1 = 0xff;					//  Enable all peripheral clocks.
	CLK->PCKENR2 = 0xff;					//  Ditto.
	CLK->CCOR = 0;							//  Turn off CCO.
	CLK->HSITRIMR = 0;						//  Turn off any HSIU trimming.
	CLK->SWIMCCR = 0;						//  Set SWIM to run at clock / 2.
	CLK->SWR = 0xe1;						//  Use HSI as the clock source.
	CLK->SWCR = 0;							//  Reset the clock switch control register.
	CLK->SWCR |= CLK_SWCR_SWEN;				//  Enable switching.
	while (CLK->SWCR & CLK_SWCR_SWBSY);		//  Pause while the clock switch is busy. */
}


void TIM2_UPD_OVF_IRQHandler(void) __interrupt (13)
{
   //  Toggle Port D, pin 4.
    TIM2->SR1 |= TIM2_SR1_UIF;               //  Reset the interrupt otherwise it will fire again straight away.
}

uint8_t r = 0;
uint8_t g = 0;
uint8_t b = 0;
//
void SetupTimer2()
{
	TIM2->PSCR = TIM2_PRESCALER_1;
	TIM2->ARRH = 0x00;
	TIM2->ARRL = 0x10;
	TIM2->CCER1 &= ~TIM2_CCER1_CC1P;    //  Active high channel 1.
	TIM2->CCER1 |= TIM2_CCER1_CC2P;    //  Active high channel 2.
	TIM2->CCER2 |= TIM2_CCER2_CC3P;    //  Active high channel 3.

    TIM2->CCER1 |= TIM2_CCER1_CC1E;    //  Enable compare mode for channel 1
	TIM2->CCER1 |= TIM2_CCER1_CC2E;    //  Enable compare mode for channel 2
	TIM2->CCER2 |= TIM2_CCER2_CC3E;    //  Enable compare mode for channel 3

    TIM2->CCMR1 |= TIM2_CCMR_OCM;    //  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
    TIM2->CCMR2 |= TIM2_CCMR_OCM;    //  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
    TIM2->CCMR3 |= TIM2_CCMR_OCM;    //  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
    TIM2->CR1 |= TIM2_CR1_CEN;       //  Finally enable the timer.
}

#define AMPL 0x10

int16_t sine(int32_t x){
//this returns -10000 to 10000 on x = 0 to 36000
	int16_t result = 0;
	x %= 35999;
	if(x <= 18000)
	result = x*(18000-x)/8100;
	if(x > 18000)
	result = (x - 36000)*(x - 18000)/8100;

	return result;
}

int32_t accum = 0;
uint32_t incmillis;
int main()
{
    __disable_interrupt();
    init_sysclock();

    SetupTimer2();
    TIM4_init();
    incmillis = millis();

    __enable_interrupt();
    while (1)
    {
		if(TIMER_TICK(inc, 1)){
		//	int32_t m;
            TIM2->CCR1H = 0x00;      //  High byte of 12,500
			TIM2->CCR1L = 0x03;      //  Low byte of 12,500
			accum += 10;
			TIMER_REFRESH(inc);
        }
    }
}
