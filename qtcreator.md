QT Creator projects tend to depend on the local configuration so I created the qtcreatorconfig script.
*.creator.user files contain the UUID of the configuration, the script reads the UUID from the local config and updates files accordingly
you may need to change ConfigDir variable in qtcreatorconfig