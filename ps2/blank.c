#include <stm8s.h>
#include <millis/tim4millis.h>
#include <delay.h>
#include <common.h>
#include <string.h>
#include "PS2X_lib.h"


#define PS2_DAT        pinport(GPIOC, 3)
#define PS2_CMD        pinport(GPIOC, 4)
#define PS2_SEL        pinport(GPIOC, 5)
#define PS2_CLK        pinport(GPIOC, 6)


#define DATA_TYPE_STICK_ANALOG ((uint8_t)0x01)
#define DATA_TYPE_BUTTON       ((uint8_t)0x02)


#define pressures   1
#define rumble      0

#define BOOTUP_PIN  9
#define LED1 A0
#define LED2 A1

PS2X_t ps2x;

int error = 0;
uint32_t lastmillis = 0;
uint32_t startmillis = 0;
uint32_t ms = 0;
uint8_t initialized = 0; //once initialized we can use the indicator leds for other purpose such as RF connection
uint8_t led1_state = 0;
uint8_t led2_state = 0;

int ledstate = 0;
float b = 0;

uint8_t last_LY = 127;
uint8_t last_LX = 127;
uint8_t last_RY = 127;
uint8_t last_RX = 127;
uint8_t analogchange = 0;

void init_sysclock()
{
    CLK->ICKR = 0;                       //  Reset the Internal Clock Register.
    CLK->ICKR |= CLK_ICKR_HSIEN;                 //  Enable the HSI.
    CLK->ECKR = 0;                       //  Disable the external clock.
    while (!(CLK->ICKR & CLK_ICKR_HSIRDY));       //  Wait for the HSI to be ready for use.
    CLK->CKDIVR = 0;                     //  Ensure the clocks are running at full speed.
    CLK->PCKENR1 = 0xff;                 //  Enable all peripheral clocks.
    CLK->PCKENR2 = 0xff;                 //  Ditto.
    CLK->CCOR = 0;                       //  Turn off CCO.
    CLK->HSITRIMR = 0;                   //  Turn off any HSIU trimming.
    CLK->SWIMCCR = 0;                    //  Set SWIM to run at clock / 2.
    CLK->SWR = 0xe1;                     //  Use HSI as the clock source.
    CLK->SWCR = 0;                       //  Reset the clock switch control register.
	CLK->SWCR |= CLK_SWCR_SWEN;                //  Enable switching.
    while (CLK->SWCR & CLK_SWCR_SWBSY);        //  Pause while the clock switch is busy. */
}


int main()
{
    uint8_t type;
    init_sysclock();
    _delay_ms(300);


    TIM4_init();
    GPIOA->DDR |= (1 << 3);
    GPIOA->CR1 |= (1 << 3);
    GPIOA->ODR |= (1 << 3);
//GPIOA->ODR &= ~(1 << 3);

    //initialize controller

    error = ps_config_gamepad2(&ps2x, PS2_CLK, PS2_CMD, PS2_SEL, PS2_DAT, pressures, rumble);
    type = ps_readType(&ps2x);
    if(type)
    {
        //digitalWrite(LED1, led1_state=1);
     //   GPIOA->ODR &= ~(1 << 3);
    }
   GPIOA->ODR &= ~(1 << 3);
//   return 1;




    //confirm bootup with led
//    GPIOB DDRB |= _BV(;
     // DDRB |= _BV(DDB7);
//    pinMode(BOOTUP_PIN, OUTPUT);
    //PORTB |= _BV(PB6);
    __enable_interrupt();

    lastmillis = millis();
    startmillis = lastmillis;
    ms = lastmillis;

    while(1)
    {
        uint8_t RX, RY, LX, LY;
        back:

        //turn of leds after 1000ms passed
        if(!initialized && millis() - lastmillis > 500)
        {
            //    digitalWrite(LED1, led1_state=0);
            //    digitalWrite(LED2, led2_state=0);
            GPIOA->ODR |= (1 << 3);
            initialized = 1;
        }

//        goto back;

        if(ledstate && millis() - lastmillis >= 10)
        {
            GPIOA->ODR |= (1 << 3);
            ledstate = 0;
        }

        if(!initialized) //skip loop if not initialized
            goto back;



        ps_read_gamepad2(&ps2x, 0, 0);
        if (ps_Newbuttonstate(&ps2x)) //handle button presses
        {
            uint8_t packeddata[8];
            uint8_t *packeddataptr;
            uint16_t data;

            //visually ack a button was pressed by blinking the light
            //PORTB |= _BV(PB6);
            GPIOA->ODR &= ~(1 << 3);
            ledstate = 1;
            lastmillis = millis();

            //if select is being held

            if(ps_Button(&ps2x, PSB_SELECT))
            {
//            digitalWrite(LED1, led1_state=1);
                /*  if(ps_Button(&ps2x, PSB_PAD_UP)){
                      //currentnumber+1;
                  } else if(ps_Button(&ps2x, PSB_PAD_DOWN)){
                     // disp.currentnumber-1;
                  } else if(ps_Button(&ps2x, PSB_PAD_LEFT)){
                    //  disp.currentnumber-0x10;
                  } else if(ps_Button(&ps2x, PSB_PAD_RIGHT)){
                    //  disp.currentnumber+0x10;
                  } */
                //rf.stopListening();
                //eeprom_write_byte((uint8_t*)PIPE_INDEX_ADDR, disp.currentnumber);
                //rf.openWritingPipe(getaddr(disp.currentnumber));
                //rf.startListening();
                goto back;
            }
            else
            {
//            digitalWrite(LED1, led1_state=0);
            }
            //if(ps2x.Button(PSB_START))         //will be TRUE as long as button is pressed

            //prepare buffer to send
            packeddataptr = packeddata;
            memset(packeddata, 0, sizeof(packeddata));

            //gather data and print to serial as binary
            data = ps_ButtonDataByte(&ps2x);
//        for(int i = 0; i < 16; i++)
//           Serial.print((int)((data & (1 << i)) && 1));
            //      Serial.print("\n");

            //fill data in buffer, not using *ptr++ for readability.
            *packeddataptr = DATA_TYPE_BUTTON;
            packeddataptr += sizeof(DATA_TYPE_BUTTON);


            *((uint16_t*)packeddataptr) = data;
            packeddataptr += sizeof(data);




            //send data
//        rf.stopListening();

//        rf.write(packeddata, sizeof(packeddata));

//        rf.startListening();
            goto back;
        }
        //determine if an analog change occurred
        analogchange = 0;
        LX = 127;
        LY = 127;
        RX = 127;
        RY = 127;
        LX = ps_Analog(&ps2x, PSS_LX);
        LY = ps_Analog(&ps2x, PSS_LY);
        RX = ps_Analog(&ps2x, PSS_RX);
        RY = ps_Analog(&ps2x, PSS_RY);

        LX = ((LX > 150 || LX < 110 ) ? LX : 127);
        LY = ((LY > 150 || LY < 110 ) ? LY : 127);
        RX = ((RX > 150 || RX < 110 ) ? RX : 127);
        RY = ((RY > 150 || RY < 110 ) ? RY : 127);

        if(
            LX != last_LX ||
            LY != last_LY ||
            RX != last_RX ||
            RY != last_RY
        )
        {
//        if(LX != 127)
            last_LX = LX;
//        if(LY != 127)
            last_LY = LY;
//        if(RX != 127)
            last_RX = RX;
//        if(RY != 127)
            last_RY = RY;
            analogchange = 1;

            GPIOA->ODR &= ~(1 << 3);
            ledstate = 1;
            lastmillis = millis();
        }


        //process the analog change
        if(analogchange)
        {
            uint8_t packeddata[8];
            uint8_t *packeddataptr;

            //blink das lightchen
            GPIOA->ODR &= ~(1 << 3);
            lastmillis = millis();
            packeddataptr = packeddata;
            memset(packeddata, 0, sizeof(packeddata));

            //fill the buffer
            *packeddataptr = DATA_TYPE_STICK_ANALOG;
            packeddataptr += sizeof(uint8_t);


            *packeddataptr = last_LX;
            packeddataptr += sizeof(last_LX);

            *packeddataptr = last_LY;
            packeddataptr += sizeof(last_LY);

            *packeddataptr = last_RX;
            packeddataptr += sizeof(last_RX);

            *packeddataptr = last_RY;
            packeddataptr += sizeof(last_RY);

        }


//  delay(50);
    }
}
