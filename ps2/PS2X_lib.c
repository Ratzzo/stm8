#include "PS2X_lib.h"
#include <math.h>
#include <stdio.h>
#include <stdint.h>

#define __AVR__

static uint8_t enter_config[]={0x01,0x43,0x00,0x01,0x00};
static uint8_t set_mode[]={0x01,0x44,0x00,0x01,0x03,0x00,0x00,0x00,0x00};
static uint8_t set_uint8_ts_large[]={0x01,0x4F,0x00,0xFF,0xFF,0x03,0x00,0x00,0x00};
static uint8_t exit_config[]={0x01,0x43,0x00,0x00,0x5A,0x5A,0x5A,0x5A,0x5A};
static uint8_t enable_rumble[]={0x01,0x4D,0x00,0x00,0x01};
static uint8_t type_read[]={0x01,0x45,0x00,0x5A,0x5A,0x5A,0x5A,0x5A,0x5A};

#define CLK_SET(ps) *ps->clk_register |= ps->clk_mask
#define CLK_CLR(ps) *ps->clk_register &= ~ps->clk_mask
#define CMD_SET(ps) *ps->cmd_register |= ps->cmd_mask
#define CMD_CLR(ps) *ps->cmd_register &= ~ps->cmd_mask
#define ATT_SET(ps) *ps->att_register |= ps->att_mask
#define ATT_CLR(ps) *ps->att_register &= ~ps->att_mask
#define DAT_CHK(ps) ((*ps->dat_register & ps->dat_mask) ? 1 : 0)


/*void CLK_SET(PS2X_t *ps);
void CLK_CLR(PS2X_t *ps);
void CMD_SET(PS2X_t *ps);
void CMD_CLR(PS2X_t *ps);
void ATT_SET(PS2X_t *ps);
void ATT_CLR(PS2X_t *ps);
uint8_t DAT_CHK(PS2X_t *ps);


void CLK_SET(PS2X_t *ps) {
//  __cli();
//    ps->_clk_pp.port->ODR |= (1 << ps->_clk_pp.pin);
    *ps->clk_register |= ps->clk_mask;
}

void CLK_CLR(PS2X_t *ps) {
//  cli();
//  *ps->_clk_oreg &= ~ps->_clk_mask;
  //  ps->_clk_pp.port->ODR &= ~(1 << ps->_clk_pp.pin);
  *ps->clk_register &= ~ps->clk_mask;
}

void CMD_SET(PS2X_t *ps) {
//  cli();
 // *ps->_cmd_oreg |= ps->_cmd_mask; // SET(*_cmd_oreg,_cmd_mask);
//    ps->_cmd_pp.port->ODR |= (1 << ps->_cmd_pp.pin);
    *ps->cmd_register |= ps->cmd_mask;
}

void CMD_CLR(PS2X_t *ps) {
 // cli();
 // *ps->_cmd_oreg &= ~ps->_cmd_mask; // SET(*_cmd_oreg,_cmd_mask);
   // ps->_cmd_pp.port->ODR &= ~(1 << ps->_cmd_pp.pin);
    *ps->cmd_register &= ~ps->cmd_mask;
}

void ATT_SET(PS2X_t *ps) {
 // cli();
//  *ps->_att_oreg |= ps->_att_mask ;
 // ps->_att_pp.port->ODR |= (1 << ps->_att_pp.pin);
  *ps->att_register |= ps->att_mask;
}

void ATT_CLR(PS2X_t *ps) {
 // cli();
 // ps->_att_pp.port->ODR &= ~(1 << ps->_att_pp.pin);
  *ps->att_register &= ~ps->att_mask;
}

uint8_t DAT_CHK(PS2X_t *ps) {
//  return (ps->_dat_pp.port->IDR & (1 << ps->_dat_pp.pin)) ? 1 : 0;
  return (*ps->dat_register & ps->dat_mask) ? 1 : 0;
} */


unsigned char _ps_gamepad_shiftinout (PS2X_t *ps, char uint8_t);
void ps_sendCommandString(PS2X_t *ps, uint8_t*, uint8_t);


/****************************************************************************************/
uint8_t ps_Newbuttonstate(PS2X_t *ps) {
  return ((ps->last_buttons ^ ps->buttons) > 0);
}

/****************************************************************************************/
uint8_t ps_Newbuttonstate2(PS2X_t *ps, unsigned int button) {
  return (((ps->last_buttons ^ ps->buttons) & button) > 0);
}

/****************************************************************************************/
uint8_t ps_ButtonPressed(PS2X_t *ps, unsigned int button) {
  return(ps_Newbuttonstate2(ps, button) & ps_Button(ps, button));
}

/****************************************************************************************/
uint8_t ps_ButtonReleased(PS2X_t *ps, unsigned int button) {
  return((ps_Newbuttonstate2(ps, button)) & ((~ps->last_buttons & button) > 0));
}

/****************************************************************************************/
uint8_t ps_Button(PS2X_t *ps, uint16_t button) {
  return ((~ps->buttons & button) > 0);
}

/****************************************************************************************/
unsigned int ps_ButtonDataByte(PS2X_t *ps) {
   return (~ps->buttons);
}

/****************************************************************************************/
uint8_t ps_Analog(PS2X_t *ps, uint8_t button) {
   return ps->PS2data[button];
}

/****************************************************************************************/
unsigned char _ps_gamepad_shiftinout (PS2X_t *ps, char byte) {
   unsigned char tmp = 0;
   unsigned char i;
   for(i=0;i<8;i++) {
      if(CHK(byte,i)) CMD_SET(ps);
      else CMD_CLR(ps);

      CLK_CLR(ps);
      _delay_us(CTRL_CLK);

      if(DAT_CHK(ps)) {
            SET(tmp,i);
      }
      //else
      //{
       //     GPIOA->ODR &= ~(1 << 3);
      //}
      //if(DAT_CHK(ps)) bitSet(tmp,i);

      CLK_SET(ps);
//#if CTRL_CLK_HIGH
//      _delay_us(100);
//#endif
   }
   CMD_SET(ps);
   _delay_us(CTRL_BYTE_DELAY);
   return tmp;
}

/****************************************************************************************/
void ps_read_gamepad(PS2X_t *ps) {
   ps_read_gamepad2(ps, 0, 0x00);
}

long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/****************************************************************************************/
uint8_t ps_read_gamepad2(PS2X_t *ps, uint8_t motor1, uint8_t motor2) {
   uint32_t temp = millis() - ps->last_read;
   uint8_t dword[9] = {0x01,0x42,0,(uint8_t)motor1,(uint8_t)motor2,0,0,0,0};
   uint8_t dword2[12] = {0,0,0,0,0,0,0,0,0,0,0,0};
   uint8_t RetryCnt;
   int i;

   if (temp > 1500) //waited to long
      ps_reconfig_gamepad(ps);

   if(temp < ps->read_delay)  //waited too short
      _delay_us((ps->read_delay - temp) * 1000 );

   if(motor2 != 0x00)
      motor2 = map(motor2,0,255,0x40,0xFF); //noting below 40 will make it spin



   // Try a few times to get valid data...

   for (RetryCnt = 0; RetryCnt < 5; RetryCnt++) {
      CMD_SET(ps);
      CLK_SET(ps);
      ATT_CLR(ps); // low enable joystick

      _delay_us(CTRL_BYTE_DELAY);
      //Send the command to send button and joystick data;

      for (i = 0; i<9; i++) {
         ps->PS2data[i] = _ps_gamepad_shiftinout(ps, dword[i]);
      }

      if(ps->PS2data[1] == 0x79) {  //if controller is in full data return mode, get the rest of data
        int i;
         for (i = 0; i<12; i++) {
            ps->PS2data[i+9] = _ps_gamepad_shiftinout(ps, dword2[i]);
         }
      }

      ATT_SET(ps); // HI disable joystick
      // Check to see if we received valid data or not.
	  // We should be in analog mode for our data to be valid (analog == 0x7_)
      if ((ps->PS2data[1] & 0xf0) == 0x70)
         break;

      // If we got to here, we are not in analog mode, try to recover...
      ps_reconfig_gamepad(ps); // try to get back into Analog mode.
      _delay_us(ps->read_delay * 1000);
   }


   // If we get here and still not in analog mode (=0x7_), try increasing the read_delay...
   if ((ps->PS2data[1] & 0xf0) != 0x70) {
      if (ps->read_delay < 10)
         ps->read_delay++;   // see if this helps out...
   }

   ps->last_buttons = ps->buttons; //store the previous ps->buttons states


   ps->buttons = *(uint16_t*)(ps->PS2data+3);   //store as one value for multiple functions
   ps->last_read = millis();
   return ((ps->PS2data[1] & 0xf0) == 0x70);  // 1 = OK = analog mode - 0 = NOK
}

/****************************************************************************************/
uint8_t ps_config_gamepad(PS2X_t *ps, pp_t *clk, pp_t *cmd, pp_t *att, pp_t *dat) {
   return ps_config_gamepad2(ps, clk, cmd, att, dat, 0, 0);
}

/****************************************************************************************/
uint8_t ps_config_gamepad2(PS2X_t *ps, pp_t *clk, pp_t *cmd, pp_t *att, pp_t *dat, uint8_t pressures, uint8_t rumble) {

    int y;
    int i;
    uint8_t temp[sizeof(type_read)];

    ps->clk_register = &clk->port->ODR;
    ps->cmd_register = &clk->port->ODR;
    ps->att_register = &clk->port->ODR;
    ps->dat_register = &clk->port->IDR;

    ps->clk_mask = (1 << clk->pin);
    ps->cmd_mask = (1 << cmd->pin);
    ps->att_mask = (1 << att->pin);
    ps->dat_mask = (1 << dat->pin);

    clk->port->DDR |= (1 << clk->pin);
    att->port->DDR |= (1 << att->pin);
    cmd->port->DDR |= (1 << cmd->pin);
    dat->port->DDR &= ~(1 << dat->pin);
    clk->port->CR1 |= (1 << clk->pin);
    att->port->CR1 |= (1 << att->pin);
    cmd->port->CR1 |= (1 << cmd->pin);
    dat->port->CR1 |= (1 << dat->pin);
    clk->port->CR2 |= (1 << clk->pin);
    att->port->CR2 |= (1 << att->pin);
    cmd->port->CR2 |= (1 << cmd->pin);
   // ps->_dat_pp.port->DDR &= ~(1 << ps->_dat_pp.pin);
    //pinMode(clk, OUTPUT); //configure ports
    //pinMode(att, OUTPUT);
    //pinMode(cmd, OUTPUT);
    //pinMode(dat, INPUT);

//  digitalWrite(dat, HIGH); //enable pull-up

  CMD_SET(ps); // SET(*_cmd_oreg,_cmd_mask);
  CLK_SET(ps);
//  ATT_SET(ps);

  //new error checking. First, read gamepad a few times to see if it's talking
  ps_read_gamepad(ps);
  ps_read_gamepad(ps);

  //see if it talked - see if mode came back.
  //If still anything but 41, 73 or 79, then it's not talking
  if(ps->PS2data[1] != 0x41 && ps->PS2data[1] != 0x73 && ps->PS2data[1] != 0x79){
#ifdef PS2X_DEBUG
    Serial.println("Controller mode not matched or no controller found");
    Serial.print("Expected 0x41, 0x73 or 0x79, but got ");
    Serial.println(ps->PS2data[1], HEX);
#endif
    return 1; //return error code 1
  }


  //try setting mode, increasing delays if need be.
  ps->read_delay = 1;
  for(y = 0; y <= 10; y++) {
    ps_sendCommandString(ps, enter_config, sizeof(enter_config)); //start config run

    //read type
    _delay_us(CTRL_BYTE_DELAY);

    CMD_SET(ps);
    CLK_SET(ps);
    ATT_CLR(ps); // low enable joystick

    _delay_us(CTRL_BYTE_DELAY);
    for (i = 0; i<9; i++) {
      temp[i] = _ps_gamepad_shiftinout(ps, type_read[i]);
    }

    ATT_SET(ps); // HI disable joystick

    ps->controller_type = temp[3];

    ps_sendCommandString(ps, set_mode, sizeof(set_mode));
    if(rumble){ ps_sendCommandString(ps, enable_rumble, sizeof(enable_rumble)); ps->en_Rumble = 1; }
    if(pressures){ ps_sendCommandString(ps, set_uint8_ts_large, sizeof(set_uint8_ts_large)); ps->en_Pressures = 1; }
    ps_sendCommandString(ps, exit_config, sizeof(exit_config));

    ps_read_gamepad(ps);

    if(pressures){
      if(ps->PS2data[1] == 0x79)
        break;
      if(ps->PS2data[1] == 0x73)
        return 3;
    }

    if(ps->PS2data[1] == 0x73)
      break;

    if(y == 10){
#ifdef PS2X_DEBUG
      Serial.println("Controller not accepting commands");
      Serial.print("mode stil set at");
      Serial.println(PS2data[1], HEX);
#endif
      return 2; //exit function with error
    }
    ps->read_delay += 1; //add 1ms to read_delay
  }
  return 0; //no error if here
}

/****************************************************************************************/
void ps_sendCommandString(PS2X_t *ps, uint8_t string[], uint8_t len) {
  int y;
#ifdef PS2X_COM_DEBUG
  uint8_t temp[len];
  ATT_CLR(ps); // low enable joystick
  delayMicroseconds(CTRL_BYTE_DELAY);

  for (int y=0; y < len; y++)
    temp[y] = _gamepad_shiftinout(string[y]);

  ATT_SET(ps); //high disable joystick
  delay(read_delay); //wait a few

  Serial.println("OUT:IN Configure");
  for(int i=0; i<len; i++) {
    Serial.print(string[i], HEX);
    Serial.print(":");
    Serial.print(temp[i], HEX);
    Serial.print(" ");
  }
  Serial.println("");
#else
  ATT_CLR(ps); // low enable joystick
  for ( y=0; y < len; y++)
    _ps_gamepad_shiftinout(ps, string[y]);
  ATT_SET(ps); //high disable joystick
  _delay_us(ps->read_delay * 1000);                  //wait a few
#endif
}

/****************************************************************************************/
uint8_t ps_readType(PS2X_t *ps) {

/*
  uint8_t temp[sizeof(type_read)];
  uint16_t i;

  sendCommandString(enter_config, sizeof(enter_config));

  _delay_us(CTRL_BYTE_DELAY);

  CMD_SET(ps);
  CLK_SET(ps);
  ATT_CLR(ps); // low enable joystick

  _delay_us(CTRL_BYTE_DELAY);

  for (i = 0; i<9; i++) {
    temp[i] = _gamepad_shiftinout(type_read[i]);
  }

  sendCommandString(exit_config, sizeof(exit_config));

  if(temp[3] == 0x03)
    return 1;
  else if(temp[3] == 0x01)
    return 2;

  return 0; */

  if(ps->controller_type == 0x03)
    return 1;
  else if(ps->controller_type == 0x01)
    return 2;
  else if(ps->controller_type == 0x0C)
    return 3;  //2.4G Wireless Dual Shock PS2 Game Controller

  return 0;
}

/****************************************************************************************/
void ps_enableRumble(PS2X_t *ps) {
  ps_sendCommandString(ps, enter_config, sizeof(enter_config));
  ps_sendCommandString(ps, enable_rumble, sizeof(enable_rumble));
  ps_sendCommandString(ps, exit_config, sizeof(exit_config));
  ps->en_Rumble = 1;
}

/****************************************************************************************/
uint8_t ps_enablePressures(PS2X_t *ps) {
  ps_sendCommandString(ps, enter_config, sizeof(enter_config));
  ps_sendCommandString(ps, set_uint8_ts_large, sizeof(set_uint8_ts_large));
  ps_sendCommandString(ps, exit_config, sizeof(exit_config));

  ps_read_gamepad(ps);
  ps_read_gamepad(ps);

  if(ps->PS2data[1] != 0x79)
    return 0;

  ps->en_Pressures = 1;
    return 1;
}

/****************************************************************************************/
void ps_reconfig_gamepad(PS2X_t *ps){
  ps_sendCommandString(ps, enter_config, sizeof(enter_config));
  ps_sendCommandString(ps, set_mode, sizeof(set_mode));
  if (ps->en_Rumble)
    ps_sendCommandString(ps, enable_rumble, sizeof(enable_rumble));
  if (ps->en_Pressures)
    ps_sendCommandString(ps, set_uint8_ts_large, sizeof(set_uint8_ts_large));
  ps_sendCommandString(ps, exit_config, sizeof(exit_config));
}
