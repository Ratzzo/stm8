//
//  This program shows how you can generate a 20 Hz signal using
//  Timer 2 and Port D, Pin 5 on the STM8S microcontroller.
//
//  This software is provided under the CC BY-SA 3.0 licence.  A
//  copy of this licence can be found at:
//
//  http://creativecommons.org/licenses/by-sa/3.0/legalcode
//
#include <stm8s.h>
#include <millis/tim4millis.h>
#include <math.h>
//
//  Setup the system clock to run at 16MHz using the internal oscillator.
//
void init_sysclock()
{
    CLK->ICKR = 0;                       //  Reset the Internal Clock Register.
    CLK->ICKR |= CLK_ICKR_HSIEN;                 //  Enable the HSI.
    CLK->ECKR = 0;                       //  Disable the external clock.
    while (!(CLK->ICKR & CLK_ICKR_HSIRDY));       //  Wait for the HSI to be ready for use.
    CLK->CKDIVR = 0;                     //  Ensure the clocks are running at full speed.
    CLK->PCKENR1 = 0xff;                 //  Enable all peripheral clocks.
    CLK->PCKENR2 = 0xff;                 //  Ditto.
    CLK->CCOR = 0;                       //  Turn off CCO.
    CLK->HSITRIMR = 0;                   //  Turn off any HSIU trimming.
    CLK->SWIMCCR = 0;                    //  Set SWIM to run at clock / 2.
    CLK->SWR = 0xe1;                     //  Use HSI as the clock source.
    CLK->SWCR = 0;                       //  Reset the clock switch control register.
	CLK->SWCR |= CLK_SWCR_SWEN;                //  Enable switching.
    while (CLK->SWCR & CLK_SWCR_SWBSY);        //  Pause while the clock switch is busy. */
}

//
//  Timer 2 Overflow handler.
//
void TIM2_UPD_OVF_IRQHandler(void) __interrupt (13)
{
   //  Toggle Port D, pin 4.
    TIM2->SR1 |= TIM2_SR1_UIF;               //  Reset the interrupt otherwise it will fire again straight away.
}

//
//  Setup Timer 2 to generate a 20 Hz interrupt based upon a 16 MHz timer.

uint8_t r = 0;
uint8_t g = 0;
uint8_t b = 0;
//
void SetupTimer2()
{
    TIM2->PSCR = TIM2_PRESCALER_1;       //  Prescaler = 8.
    TIM2->ARRH = 0x00;       //  High byte of 50,000 . (freq)
    TIM2->ARRL = 0xFF;       //  Low byte of 50,000.
    TIM2->CCER1 |= TIM2_CCER1_CC1P;    //  Active high.
    TIM2->CCER1 |= TIM2_CCER1_CC2P;    //  Active high.
    TIM2->CCER2 |= TIM2_CCER2_CC3P;    //  Active high.
    TIM2->CCER1 |= TIM2_CCER1_CC1E;    //  Enable compare mode for channel 1
    TIM2->CCER1 |= TIM2_CCER1_CC2E;    //  Enable compare mode for channel 1
    TIM2->CCER2 |= TIM2_CCER2_CC3E;    //  Enable compare mode for channel 1
    TIM2->CCMR1 |= TIM2_CCMR_OCM;    //  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
    TIM2->CCMR2 |= TIM2_CCMR_OCM;    //  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
    TIM2->CCMR3 |= TIM2_CCMR_OCM;    //  PWM Mode 1 - active if counter < CCR1, inactive otherwise.
  //  TIM2->IER |= TIM2_IER_UIE;       //  Enable the update interrupts.
    TIM2->CR1 |= TIM2_CR1_CEN;       //  Finally enable the timer.
}

//
//  Main program loop.
//

#define AMPL 10

float accum = 0;
uint32_t incmillis;
int main()
{
    __disable_interrupt();
    init_sysclock();

    //GPIOD->ODR ^= (1 << 3);
    SetupTimer2();
    TIM4_init();
    incmillis = millis();

    __enable_interrupt();
    while (1)
    {
        if(millis() - incmillis > 100){
            TIM2->CCR1H = 0x00;      //  High byte of 12,500
            TIM2->CCR1L = r;      //  Low byte of 12,500
            TIM2->CCR2H = 0x00;      //  High byte of 12,500
            TIM2->CCR2L = g;      //  Low byte of 12,500
            TIM2->CCR3H = 0x00;      //  High byte of 12,500
            TIM2->CCR3L = b;      //  Low byte of 12,500
            accum += 0.05f;
            r = (AMPL + AMPL*sinf(accum + PI/1));
            g = (AMPL + AMPL*sinf(accum + PI/2));
            b = (AMPL + AMPL*sinf(accum + PI/3));
            incmillis = millis();
        }
    }
}
